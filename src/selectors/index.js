import queryString from 'query-string';
import { createSelector } from 'reselect';

export const currentUser = state => state.currentUser;
export const token = state => state.token;
export const id = (_, props) => Number.parseInt(props.match.params.id);
export const search = (_, props) => queryString.parse(props.history.location.search);
export const searchQuery = formName => state => state[formName].query.replace(/[()-\s]/g, '').replace(/^\+/, '^\\+');
export const calls = (searchQuery, userCalls) => createSelector(
  searchQuery,
  userCalls,
  (searchQuery, userCalls) => userCalls.filter(c => {
    const conditions = [];
    if (searchQuery !== '') {
      const re = new RegExp(searchQuery, 'i');
      const addCondition = condition => conditions.push(re.test(condition));
      addCondition(c.number);
      if (c.incoming) addCondition(c.answerNumber);
      if (!c.incoming) addCondition(c.localNumber.number);
      if (c.contact) addCondition(c.contact.name);
      if (c.contact) addCondition(c.contact.number);
      if (c.userName) addCondition(c.userName);
      return conditions.reduce((memo, r) => memo || r, false);
    } else return true;
  }),
);
