import React from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import { Control } from 'react-redux-form';

const InputSelect = ({ model, afterChange, ...rest }) =>
  <Control
    model={model}
    component={props =>
      <SelectField
        value={props.value}
        errorText={props.touched && props.error}
        {...props}
        onChange={(event, index, value) => {
          props.onChange(value);
          afterChange && afterChange(value);
        }}
      >
        {rest.children}
      </SelectField>
    }
    controlProps={rest}
  />;

InputSelect.propTypes = {
  model: PropTypes.string.isRequired,
  value: PropTypes.any,
  touched: PropTypes.any,
  error: PropTypes.any,
  onChange: PropTypes.any,
  afterChange: PropTypes.any,
};

export default InputSelect;
