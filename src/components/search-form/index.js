import React from 'react';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import VectorIcon from 'vector-icon';

const SearchForm = (props) =>
  <Form
    model="search"
    style={{
      margin: 0,
      padding: 0,
      position: 'relative',
      top: 6,
    }}
    {...props}
  >
    <InputText
      model=".query"
      hintText={[<span key="search">Search</span>, <VectorIcon key="search-icon" name="search" />]}
      hintStyle={{
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontSize: 12,
        // color: '#555759',
      }}
      underlineStyle={{ bottom: 0, width: 0 }}
      underlineFocusStyle={{ width: '100%' }}
      style={{ width: 226 }}
    />
  </Form>;

export default SearchForm;
