import React from 'react';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';

const DialogBoxButton = ({ style = {}, ...rest }) =>
  <FlatButton
    style={{
      flexGrow: 1,
      height: 71,
      ...style,
    }}
    hoverColor="#eaeaea"
    {...rest}
  />;

DialogBoxButton.propTypes = {
  style: PropTypes.object,
};

export default DialogBoxButton;
