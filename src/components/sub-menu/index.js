import React from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import VectorIcon from 'vector-icon';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';


const items = {
  admin: [
    { name: 'Managers', path: '/' },
    { name: 'Admins', path: '/admins' },
  ],
  manager: [
    { name: 'Rep', path: '/' },
    { name: 'Call logs', path: '/call-logs' },
    { name: 'Usage', path: '/manager-usage' },
    { name: 'Add-Ons', path: '/add-ons' },
    { name: 'Webhooks', path: '/webhooks' },
  ],
  rep: [
    { name: 'Call logs', path: '/' },
    { name: 'Contacts', path: '/contacts' },
  ],
};

const SubMenu = ({
  role,
  path,
}) =>
  <div className="submenu">
    <div className="submenu-desktop">
      {items[role].map(i =>
        <Link
          key={i.path}
          to={i.path}
          className={cx({ active: path === i.path })}
        >
          {i.name}
        </Link>
      )}
    </div>
    <div className="submenu-mobile">
      <IconMenu
        iconButtonElement={<IconButton><VectorIcon name="submenu" /></IconButton>}
        listStyle={{ backgroundColor: '#56c2cd', borderRadius: '0 0 0 5px' }}
        menuItemStyle={{ color: '#fff', fontSize: 13 }}
        animated={false}
        style={{backgroundColor: '#56c2cd'}}
      >
          {items[role].map(i =>
            <MenuItem
              primaryText={i.name}
              key={i.path}
              containerElement={<Link to={i.path} />}
            />  
          )}
        </IconMenu>
      </div>
  </div>;

SubMenu.propTypes = {
  role: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default SubMenu;
