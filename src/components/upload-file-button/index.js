import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const UploadFileButton = ({
  upload,
  label = 'import',
  id = 'upload-file',
  accept = '.csv,.vcf',
  ...rest
}) => <span>
  <input
    type="file"
    id={id}
    accept={accept}
    style={{ display: 'none' }}
    onChange={e => {
      const file = e.target.files[0];
      if (file) {
        const fd = new FormData();
        fd.append('file', file);
        upload(fd);
      }
      return false;
    }}
    {...rest}
  />
  <StyledLabel htmlFor={id}>
    {label}
  </StyledLabel>
</span>;

UploadFileButton.propTypes = {
  upload: PropTypes.func.isRequired,
  label: PropTypes.string,
  id: PropTypes.string,
  accept: PropTypes.string,
};

const StyledLabel = styled.label`
  text-transform: uppercase;
  font-weight: 500;
  cursor: pointer;
`;

export default UploadFileButton;
