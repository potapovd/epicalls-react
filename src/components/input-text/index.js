import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import { Control } from 'react-redux-form';

const InputText = ({ model, validators, ...rest }) =>
  <Control
    model={model}
    validators={validators}
    component={TextField}
    controlProps={rest}
  />;

InputText.propTypes = {
  model: PropTypes.string.isRequired,
  validators: PropTypes.object,
  messages: PropTypes.object,
};

export default InputText;
