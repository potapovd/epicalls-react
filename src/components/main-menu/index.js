import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import VectorIcon from 'vector-icon';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';

const menuItems = {
  common: [
    { title: 'My Account', iconName: 'user', link: '/my-account' },
  ],
  admin: [],
  manager: [
    { title: 'My Numbers', iconName: 'myNumbers', link: '/my-numbers' },
  ],
  user: [
    { title: 'My Numbers', iconName: 'myNumbers', link: '/my-numbers' },
  ],
};

const MainMenu = ({ role, logout, backToMainAccount, loggedAs }) => <IconMenu
  iconButtonElement={<IconButton><VectorIcon name="menu" /></IconButton>}
  anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
  targetOrigin={{ horizontal: 'right', vertical: 'top' }}
  listStyle={{ backgroundColor: '#56c2cd', borderRadius: '0 0 0 5px' }}
  menuItemStyle={{ color: '#fff', fontSize: 13 }}
  animated={false}
>
  {(menuItems.common.concat(menuItems[role] || menuItems.user)).map(item =>
    <MenuItem
      key={item.link}
      primaryText={item.title}
      containerElement={<Link to={item.link} />}
      innerDivStyle={menuItemInnerDivStyle}
      leftIcon={<VectorIcon name={item.iconName} style={mainMenuIconStyle} />}
    />
  )}
  {loggedAs && <MenuItem
    key="backToMainAccount"
    primaryText={'Return to my Account'}
    innerDivStyle={menuItemInnerDivStyle}
    onTouchTap={backToMainAccount}
    leftIcon={<VectorIcon name="signOut" style={mainMenuIconStyle} />}
  />}
  <MenuItem
    primaryText="Sign out"
    onTouchTap={logout}
    innerDivStyle={{ padding: menuItemInnerDivStyle.padding }}
    leftIcon={<VectorIcon name="signOut" style={mainMenuIconStyle} />}
  />
</IconMenu>;

const mainMenuIconStyle = {
  width: 13,
  height: 13,
  margin: 0,
  top: 17,
  left: 21,
};

const menuItemInnerDivStyle = {
  padding: '0 16px 0 46px',
  borderBottom: '1px solid rgba(255, 255, 255, .3)',
};

MainMenu.propTypes = {
  role: PropTypes.string,
  logout: PropTypes.func.isRequired,
  backToMainAccount: PropTypes.func.isRequired,
  loggedAs: PropTypes.string,
};

export default MainMenu;
