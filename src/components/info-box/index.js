import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import styled from 'styled-components';

const InfoBox = ({
  open,
  children,
  ...rest
}) => {
  return <Dialog
    className="five-px-modal-border-radius"
    open={open}
    bodyStyle={{
      padding: '0',
      borderRadius: '5px',
    }}
    actionsContainerStyle={{
      padding: 0,
      display: 'flex',
    }}
    {...rest}
  >
    <GradientLine />
    <DialogBody>
      {children}
    </DialogBody>
  </Dialog>;
};

const DialogBody = styled.div`
  padding: 2rem 41px;
  border-bottom: 1px solid #eee;
`;

const GradientLine = styled.div`
  width: 100%;
  height: 5px;
  background-image: linear-gradient(225deg, #56c2cd, #83a4d5);
`;

InfoBox.propTypes = {
  open: PropTypes.bool.isRequired,
  children: PropTypes.any,
};

export default InfoBox;
