import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Redirect } from 'react-router';

import Reps from './pages/reps';
import Managers from './pages/managers';
import Admins from './pages/admins';
import Signup from './pages/signup';
import Login from './pages/login';
import PasswordReset from './pages/password-reset';
import ResetPassword from './pages/reset-password';
import CallLogs from './pages/call-logs';
import ManagerCallLogs from './pages/manager-call-logs';
import Contacts from './pages/contacts';
import MyNumbers from './pages/my-numbers';
import ManagerNumbers from './pages/manager-numbers';
import ManagerUsage from './pages/manager-usage';
import MyAccount from './pages/my-account';
import ChangeName from './pages/my-account/change-name';
import ChangeEmail from './pages/my-account/change-email';
import ChangePassword from './pages/my-account/change-password';
import ConfirmEmail from './pages/confirm-email';
import AddOns from './pages/add-ons';
import Webhooks from './pages/webhooks';
import Inactive from './pages/inactive';

import NotFound from './pages/not-found';

import makeLayoutRoute from './utils/make-layout-route';
import PrivateLayout from './layouts/private';

const PrivateRoute = makeLayoutRoute(PrivateLayout, { loginComponent: Login });

const HomeRedirect = () => <Redirect push to="/" />;

const Routes = () => (
  <Switch>
    <PrivateRoute exact path="/" page={CallLogs} managerPage={Reps} adminPage={Managers} />
    <PrivateRoute exact path="/admins" page={HomeRedirect} managerPage={HomeRedirect} adminPage={Admins} />
    <PrivateRoute exact path="/my-account/change-name" page={ChangeName} />
    <PrivateRoute exact path="/my-account/change-email" page={ChangeEmail} />
    <PrivateRoute exact path="/my-account/change-password" page={ChangePassword} />
    <PrivateRoute exact path="/my-account" page={MyAccount} />
    <PrivateRoute exact path="/call-logs" page={CallLogs} managerPage={ManagerCallLogs} adminPage={HomeRedirect} />
    <PrivateRoute exact path="/contacts" page={Contacts} managerPage={HomeRedirect} adminPage={HomeRedirect} />
    <PrivateRoute exact path="/my-numbers" page={MyNumbers} managerPage={ManagerNumbers} adminPage={HomeRedirect} />
    <PrivateRoute exact path="/manager-usage/:year?/:month?" page={HomeRedirect} managerPage={ManagerUsage} adminPage={HomeRedirect} />
    <PrivateRoute exact path="/add-ons" page={HomeRedirect} managerPage={AddOns} adminPage={HomeRedirect} />
    <PrivateRoute exact path="/confirm-email/:id/:email" page={ConfirmEmail} />
    <PrivateRoute exact path="/webhooks" page={HomeRedirect} managerPage={Webhooks} />
    <Route path="/signup" component={Signup} />
    <Route path="/reset-password" component={ResetPassword} />
    <Route path="/password-reset/:id/:email" component={PasswordReset} />
    <Route path="/inactive" component={Inactive} />
    <Route component={NotFound} />
  </Switch>
);

export default Routes;
