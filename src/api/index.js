import jwtDecode from 'jwt-decode';
import { createCookie } from 'utils/manage-cookies';
import {
  makeActionCreator,
  makeIdActionCreator,
  makePostActionCreator,
  makePostIdActionCreator,
  makeIdDeleteActionCreator,
  makeSendFileActionCreator,
  makeReducer,
  makeRawReducer,
} from './helpers';

import {
  logout,
  updateToken,
  loginAs,
} from 'actions';

import * as actionTypes from 'constants/action-types';

const endpoint = process.env.ENDPOINT;
const makeAction = makeActionCreator(endpoint, fetch);
const makeIdAction = makeIdActionCreator(endpoint, fetch);
const makePostAction  = makePostActionCreator(endpoint, fetch);
const makeIdPostAction = makePostIdActionCreator(endpoint, fetch);
const makeIdDeleteAction = makeIdDeleteActionCreator(endpoint, fetch);
const makeSendFileAction = makeSendFileActionCreator(endpoint, fetch);

const callToInital = {
  localNumbers: [],
  contact: {},
};

const api = {
  actions: {
    // Users
    login: makePostAction('user_token', { after: saveToken }),
    signup: makePostAction('signup'),
    passwordReset: makeIdPostAction('password_resets', { method: 'PATCH' }),
    resetPassword: makePostAction('password_resets'),
    currentUser: makeAction('currentUser', 'current_user', { handleFailed: resetAuth }),
    updateCurrentUser: makePostAction('current_user', { method: 'PATCH', after: reloadUser }),
    updateCurrentUserPassword: makePostAction('current_user_password'),

    // Emails
    addEmail: makePostAction('emails', { after: reloadUser }),
    changeEmail: makeIdPostAction('emails', { method: 'PATCH', after: reloadUser }),
    removeEmail: makeIdDeleteAction('emails', { after: reloadUser }),
    confirmEmail: makeIdPostAction('confirm_email', { after: reloadUser }),

    // Reps
    createRep: makePostAction('create_rep', { after: reloadUser }),
    removeRep: makeIdDeleteAction('remove_rep', { after: reloadUser }),
    removeRepLocalNumbers: makeIdDeleteAction('remove_rep_local_numbers', { after: reloadUser }),
    removeRepLocalNumber: makeIdDeleteAction('remove_rep_local_number', { after: reloadUser }),
    updateRep: makeIdPostAction('update_rep', { method: 'PATCH', after: reloadUser }),
    repToken: makeIdPostAction('rep_token', { after: loginWithNewToken }),
    callTo: makeIdAction('callTo', 'call_to'),

    // Managers
    createManager: makePostAction('create_manager', { after: reloadUser }),
    removeManager: makeIdDeleteAction('remove_manager', { after: reloadUser }),
    updateManager: makeIdPostAction('update_manager', { method: 'PATCH', after: reloadUser }),
    managerToken: makeIdPostAction('manager_token', { after: loginWithNewToken }),

    // Admins
    createAdmin: makePostAction('create_admin', { after: reloadUser }),

    // Calls
    updateCall: makeIdPostAction('calls', { method: 'PATCH', after: reloadUser }),
    newCall: makeAction('newCall', 'new_call'),

    // Local Numbers
    addLocalNumber: makePostAction('local_numbers', { after: reloadUser }),
    removeLocalNumber: makeIdDeleteAction('local_numbers', { after: reloadUser }),
    updateLocalNumber: makeIdPostAction('update_rep_local_number', { method: 'PATCH', after: reloadUser }),
    availableLocalNumber: makePostAction('local_numbers/available'),

    // Validate number
    validateNumber: makePostAction('validate_number'),

    // Forwarding Number
    updateForwardingNumber: makeIdPostAction('forwarding_numbers', { method: 'PATCH', after: reloadUser }),

    // Contacts
    addContact: makePostAction('contacts'),
    removeContact: makeIdDeleteAction('contacts', { after: reloadUser }),
    parseContactsFile: makeSendFileAction('import_contacts/parse_file'),
    importContacts: makePostAction('import_contacts/import', { after: reloadUser }),

    // Stats
    stats: makeAction('stats', 'stats'),

    // Add Ons
    addOns: makeAction('addOns', 'add_ons'),
    createAddOn: makePostAction('add_ons', { after: reloadAddOns }),
    removeAddOn: makeIdDeleteAction('add_ons', { after: reloadAddOns }),

    // Validate forwarding number
    makeValidationCall: makePostAction('make_validation_call'),
    checkConfirmationCode: makePostAction('confirm_forwarding_number', { after: reloadUser }),

    // Webhooks
    createWebhook: makePostAction('webhooks', { after: reloadUser }),
    removeWebhook: makeIdDeleteAction('webhooks', { after: reloadUser }),
    testWebhook: makeIdPostAction('test_webhook'),

    // WebhookActions
    webhookActions: makeAction('webhookActions', 'webhook_actions'),
  },
  reducers: {
    users: makeReducer('users'),
    stats: makeReducer('stats'),
    webhookActions: makeReducer('webhookActions'),
    addOns: makeRawReducer('addOns', {
      defaultState: [],
      reducer: (state = [], action) => {
        if (action.type === actionTypes.TOGGLE_ADD_ON) {
          return state.map(addOn => ({
            ...addOn,
            installed: addOn.sid === action.payload.sid ? !addOn.installed : addOn.installed,
          }));
        }
        return state;
      },
    }),
    currentUser: makeRawReducer('currentUser', {
      reducer: (state = {}, action) => {
        if (action.type === actionTypes.LOGOUT) {
          return null;
        }
        return state;
      },
    }),
    newCall: makeRawReducer('newCall', {
      reducer: (state = null, action) => {
        if (action.type === actionTypes.HANGUP_CALL) {
          return null;
        }
        return state;
      },
    }),
    callTo: makeRawReducer('callTo', {
      defaultState: callToInital,
      reducer: (state, action) => {
        if (action.type === actionTypes.HANGUP_CALL) {
          return callToInital;
        }
        return state;
      },
    }),
  },
};

function saveToken({ data, dispatch }) {
  const token = jwtDecode(data.jwt);
  createCookie('token', data.jwt, token.exp);
  return dispatch(updateToken(data.jwt));
}

function loginWithNewToken({ data, dispatch }) {
  dispatch(loginAs(jwtDecode(data.jwt).role));
  return dispatch(updateToken(data.jwt));
}

function resetAuth({ dispatch, error }) {
  return dispatch(logout());
}

function reloadUser({ dispatch, data }) {
  dispatch(api.actions.currentUser.get());
  return data;
}

function reloadAddOns({ dispatch, data }) {
  dispatch(api.actions.addOns.get());
  return data;
}

export default api;
