import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import RoundedRaisedButton from 'components/rounded-raised-button';

const AddAnAdmin = ({
  submit,
}) => <div className="addWidget">
  <h2><span className="bolder">Add</span> An Admin</h2>
  <Form
    model="admin"
    onSubmit={submit}
  >
    <InputText
      model=".admin.firstName"
      hintText="Full Name"
      fullWidth
      required
    />
    <br />
    <InputText
      model=".admin.email"
      hintText="Email"
      fullWidth
      required
    />
    <div>
      <ErrorBox model="admin.commonErrors" show />
    </div>
    <div style={{ height: 30 }} />
    <RoundedRaisedButton
      type="submit"
      label="Send Invite"
      fullWidth
      primary
    />
  </Form>
</div>;

AddAnAdmin.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default smartify(AddAnAdmin);
