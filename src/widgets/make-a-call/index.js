import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import FlatButton from 'material-ui/FlatButton';
import RoundedRaisedButton from 'components/rounded-raised-button';
import ErrorBox from 'components/error-box';
import CallModal from './call-modal';
import VectorIcon from 'vector-icon';
import NotifyBox from 'components/notify-box';
import AlertBox from 'components/alert-box';
import DialPad from 'components/dial-pad';
import InputSelect from 'components/input-select';
import MenuItem from 'material-ui/MenuItem';

const MakeACall = ({
  submit,
  addContact,
  clientNumber,
  twilioProcessLog,
  purchaseModalOpen,
  togglePurchaseModal,
  purchaseInfo,
  purchaseNumber,
  resetCallForm,
  myNewContact,
  setMyNewContact,
  dialPadOpen,
  openDialPad,
  closeDialPad,
  clientNumberError,
  setCountryCode,
  setCountryCodeFromNumber,
  callTo,
}) =>
  <div>
    <h2>Make a <span className="bolder">Call</span></h2>
    <DialPad open={dialPadOpen} onRequestClose={closeDialPad} />
    <Form
      id="make-a-call-form"
      model="call"
      onSubmit={submit}
      style={{
        position: 'relative',
      }}
    >
      <VectorIcon
        name="keyboard"
        style={{ position: 'absolute', top: 90, right: 18, zIndex: 100, cursor: 'pointer' }}
        onClick={openDialPad}
      />
      <InputSelect
        model=".countryCode"
        floatingLabelText="Country Code"
        defaultValue={1}
        afterChange={setCountryCode(clientNumber)}
        required
        fullWidth
      >
        <MenuItem value={1} primaryText="+1 USA and Canada" />
        <MenuItem value={44} primaryText="+44 Great Britain" />
        <MenuItem value={61} primaryText="+61 Australia" />
        <MenuItem value={353} primaryText="+353 Ireland" />
        <MenuItem value={57} primaryText="+57 Colombia" />
      </InputSelect>
      <InputText
        model=".number"
        hintText={<span><VectorIcon name="phone" style={{ position: 'relative', marginRight: 6, marginLeft: 4 }} />Phone Number</span>}
        onChange={setCountryCodeFromNumber}
        validators={{
          correctNumber: val => /^[0-9()+-\s]+$/.test(val),
          hasCountryCode: val => /^\+(1|44|61|353|57)/.test(val),
        }}
        defaultValue="+1"
        errorText={clientNumberError}
        fullWidth
        required
      />
      <ErrorBox model="call.commonErrors" show />
    </Form>
    <Form
      id="add-contact-form"
      model="contact"
      onSubmit={addContact(clientNumber)}
    >
      <InputText
        model=".name"
        hintText={<span><UserIcon name="user" style={{ position: 'relative', marginRight: 4, marginLeft: 4 }} />Full Name <span style={{ fontSize: 12 }}>optional</span></span>}
        fullWidth
      />
      <ErrorBox model="contact.commonErrors" show />
    </Form>
    <br /><br />
    <RoundedRaisedButton
      type="submit"
      form="make-a-call-form"
      label={<span><WhiteCall name="call" style={{ marginRight: 6 }} />New Call</span>}
      fullWidth
      primary
    />
    <br /><br />
    <FlatButton
      type="submit"
      form="add-contact-form"
      label={<span><b>Add</b> to Contacts</span>}
      icon={<VectorIcon name="plus" />}
      labelStyle={{
        textTransform: 'none',
        fontWeight: 400,
        fontSize: 16,
      }}
      hoverColor="transparent"
      disableTouchRipple
      fullWidth
    />
    <div style={{ height: '2rem' }} />
    <div>
      {twilioProcessLog.map((message, i) =>
        <p key={i}>&gt; {message}</p>
      )}
    </div>
    {!!callTo.to && <CallModal />}
    <NotifyBox
      open={!!myNewContact}
      onRequestClose={() => setMyNewContact(null)}
    >
      <b>{myNewContact && myNewContact.name}</b> added!
    </NotifyBox>
    <AlertBox
      open={purchaseModalOpen}
      onRequestClose={togglePurchaseModal}
      yesAction={purchaseNumber}
    >
      {purchaseInfo.error
        ? purchaseInfo.error
        : `You dont have a phone with the area code ${purchaseInfo.areaCode} in ${purchaseInfo.country}, would you like to purchase a number?`
      }
    </AlertBox>
  </div>;

const WhiteCall = styled(VectorIcon)`
  path {
    fill: #fff;
  }
`;

const UserIcon = styled(VectorIcon)`
  > path {
    fill: #000;
    opacity: .4;
  }
`;

MakeACall.propTypes = {
  submit: PropTypes.func.isRequired,
  addContact: PropTypes.func.isRequired,
  clientNumber: PropTypes.string,
  twilioProcessLog: PropTypes.array,
  purchaseModalOpen: PropTypes.bool.isRequired,
  togglePurchaseModal: PropTypes.func.isRequired,
  purchaseInfo: PropTypes.object,
  purchaseNumber: PropTypes.func.isRequired,
  resetCallForm: PropTypes.func.isRequired,
  myNewContact: PropTypes.any,
  setMyNewContact: PropTypes.func.isRequired,
  dialPadOpen: PropTypes.bool.isRequired,
  openDialPad: PropTypes.func.isRequired,
  closeDialPad: PropTypes.func.isRequired,
  clientNumberError: PropTypes.string,
  setCountryCode: PropTypes.func.isRequired,
  setCountryCodeFromNumber: PropTypes.func.isRequired,
  callTo: PropTypes.object,
};

export default smartify(MakeACall);
