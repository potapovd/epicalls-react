import { compose } from 'redux';
import { connect } from 'react-redux';
import { withState, withHandlers, lifecycle } from 'recompose';
import { createSelector, createStructuredSelector } from 'reselect';
import { hangUpCall, addTwilioProcessLog, clearTwilioProcessLog } from 'actions';
import api from 'api';

const areaCode = state => +state.callTo.areaCode;
const country = state => state.callTo.country;
const numbers = state => state.callTo.localNumbers;

const localNumbers = createSelector(
  areaCode,
  country,
  numbers,
  (areaCode, country, numbers) => numbers.map(l => ({
    local: (l.country === country && l.areaCode === areaCode),
    ...l,
  }))
);

const sharedLocalNumber = createSelector(
  areaCode,
  country,
  localNumbers,
  (areaCode, country, localNumbers = []) => {
    const sln = localNumbers.find(l => l.shared && l.areaCode === areaCode && l.country === country);
    return sln ? sln.number : undefined;
  }
);

const localNumber = createSelector(
  areaCode,
  country,
  localNumbers,
  (areaCode, country, localNumbers = []) => {
    const ln = localNumbers.find(l => l.local && !l.shared && l.areaCode === areaCode && l.country === country);
    return ln ? ln.number : undefined;
  }
);

const selector = createStructuredSelector({
  currentUserId: state => state.currentUser.id,
  newCall: state => state.callTo,
  localNumbers,
  sharedLocalNumber,
  localNumber,
});

const mapDispatchToProps = (dispatch, props) => ({
  setupTwilio: () => {
    window.Twilio.Device.ready(device => dispatch(addTwilioProcessLog('Twilio.Device Ready!')));
    window.Twilio.Device.error(error => dispatch(addTwilioProcessLog('Twilio.Device Error: ' + error.message)));
    window.Twilio.Device.connect(conn => {
      props.setCallStatus(true);
      props.resetCallTimer();
      dispatch(addTwilioProcessLog('Successfully established call!'));
    });
    window.Twilio.Device.disconnect(conn => {
      dispatch(hangUpCall());
      props.setCallStatus(false);
      dispatch(addTwilioProcessLog('Call ended.'));
    });
  },
  makeACall: (token, number, currentUserId) => () => {
    dispatch(clearTwilioProcessLog());
    const callParams = {
      From: props.callFromNumber,
      To: number,
      Record: props.recordCall,
      UserId: currentUserId,
    };
    // make a call after Twilio device is ready
    window.Twilio.Device.setup(token);
    const callIntID = setInterval(() => {
      if (window.Twilio.Device.status() === 'ready') {
        window.Twilio.Device.connect(callParams);
        clearInterval(callIntID);
      }
    }, 200);
  },
  hangUp: () => {
    dispatch(addTwilioProcessLog('Hanging up...'));
    window.Twilio.Device.disconnectAll();
    if (!props.callStatus) {
      dispatch(hangUpCall());
    }
  },
  purchaseNumber: (areaCode, country, clientNumber) => {
    dispatch(api.actions.availableLocalNumber({ localNumber: { areaCode, country } }))
      .then(({ number }) => dispatch(api.actions.addLocalNumber({ localNumber: { number } }))
        .then(_ => dispatch(api.actions.callTo.get(clientNumber))));
  },
});

export default compose(
  withState('callTimer', 'setCallTimer', 0),
  withState('timerIntervalID', 'setTimerIntervalID', null),
  withState('recordCall', 'setRecordCall', true),
  withState('callStatus', 'setCallStatus', false),
  withState('callFromNumber', 'setCallFromNumber', null),
  withHandlers({
    resetCallTimer: ({ setCallTimer }) => () => setCallTimer(0),
    incrementCallTimer: ({ setCallTimer, callTimer }) => () => setCallTimer(callTimer + 1000),
    selectCallFromNumber: ({ setCallFromNumber }) => (e, key, payload) => setCallFromNumber(payload),
    toggleRecordCall: ({ setRecordCall }) => (e, isInputChecked) => setRecordCall(isInputChecked),
  }),
  connect(selector, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      this.props.setupTwilio();
      const timerIntervalID = setInterval(this.props.incrementCallTimer, 1000);
      this.props.setTimerIntervalID(timerIntervalID);
      if (this.props.localNumber || this.props.sharedLocalNumber) {
        this.props.setCallFromNumber(this.props.localNumber || this.props.sharedLocalNumber);
      }
    },
    componentWillUnmount() {
      clearInterval(this.props.timerIntervalID);
    },
    componentWillReceiveProps(nextProps) {
      const nextNumber = nextProps.localNumber || nextProps.sharedLocalNumber;
      const thisNumber = this.props.localNumber || this.props.sharedLocalNumber;
      if (nextNumber !== thisNumber) this.props.setCallFromNumber(nextNumber);
      if (nextProps.callStatus !== this.props.callStatus) this.props.resetCallTimer();
    },
  }),
);
