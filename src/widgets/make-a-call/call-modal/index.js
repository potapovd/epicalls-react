import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import VectorIcon from 'vector-icon';
import InputText from 'components/input-text';
import { Form } from 'react-redux-form';
import styled from 'styled-components';
import smartify from './smartify';
import moment from 'moment';
import Toggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CountryFlag from 'components/country-flag';
import 'moment-timezone';

const buttonStyle = {
  flexGrow: 1,
  height: 71,
  fontWeight: 400,
  fontSize: 13,
  width: '33.3%',
};

const CallModal = ({
  newCall,
  makeACall,
  hangUp,
  callTimer,
  toggleRecordCall,
  recordCall,
  selectCallFromNumber,
  callFromNumber,
  callStatus,
  localNumbers,
  sharedLocalNumber,
  localNumber,
  currentUserId,
  purchaseNumber,
}) => {
  function localNumberToText(localNumber) {
    const { number, shared, local } = localNumber;
    let sharedLocal = [];
    if (shared) sharedLocal.push('shared');
    if (local) sharedLocal.push('local');
    sharedLocal = sharedLocal.join(', ');
    if (sharedLocal.length !== 0) return `${number} (${sharedLocal})`;
    return number;
  }

  return <Dialog
    title={<ModalTitle>
      <div>
        {newCall.contact ? newCall.contact.name : 'Client Number'}
        <span className="number" style={{ display: 'inline-block' }}>{newCall.to}</span>
      </div>
      <div className="call-timer">
        <div>Ongoing Call:</div>
        <div className="timer">
          {!callStatus && '00:00:00'}
          {callStatus && moment.utc(callTimer).format('HH:mm:ss')}
        </div>
      </div>
    </ModalTitle>}
    actions={[
      <FlatButton
        key="handUp"
        label="Hang Up"
        onTouchTap={hangUp}
        style={{
          ...buttonStyle,
          color: '#fff',
        }}
        backgroundColor="#e4583e"
        hoverColor="#bc4934"
        icon={<VectorIcon name="hangUp" />}
      />,
      <FlatButton
        key="mute"
        label="Mute"
        onClick={() => false}
        style={buttonStyle}
        backgroundColor="#f4f4f4"
        hoverColor="#eaeaea"
        icon={<VectorIcon name="mute" />}
      />,
      <FlatButton
        key="call"
        label="Call"
        onTouchTap={makeACall(newCall.token, newCall.to, currentUserId)}
        style={{
          ...buttonStyle,
          color: '#fff',
        }}
        backgroundColor="#76CD1A"
        hoverColor="#43A047"
        icon={<WhiteCall name="call" />}
        disabled={callStatus}
      />,
    ]}
    open
    actionsContainerStyle={{
      padding: 0,
      display: 'flex',
    }}
    titleStyle={{
      height: 110,
      fontSize: 24,
      borderBottom: '1px solid #eee',
      padding: '24px 41px 20px',
    }}
    bodyStyle={{
      padding: '2rem 41px',
      height:"100%"
    }}
    modal = {true}
    autoScrollBodyContent={true}
    contentStyle={{width: "90%"}}
  >
    <CallInfo>

      <div>
        <div>
          <VectorIcon name="pin" style={{ position: 'relative', top: 2 }} />
          <span style={{ margin: '0 8px' }}>{newCall.geoName}, {newCall.country}</span>
          {newCall.country && <CountryFlag country={newCall.country} style={{ position: 'relative', top: 2 }} />}
        </div>
        <div>
          <div style={{ marginTop: 4 }}>
            <span style={{ color: '#a1a1a1', fontSize: 14 }}>Local Time:</span>
            {' '}
            {moment.tz(new Date(), newCall.numberTimezone).format('h:mm:ss A')}
          </div>
        </div>
        <div className="recordcallblock">
          <Toggle
            label="Record Call"
            labelPosition="right"
            toggled={recordCall}
            onToggle={toggleRecordCall}
            disabled={callStatus}
          />
        </div>
      </div>

      <div>
        <div>
          Number of Calls
        </div>
        <div>
          <div style={{ fontSize: 24, fontWeight: 'normal' }}>
            {newCall.numberOfCalls + ' Call' + (newCall.numberOfCalls !== 1 ? 's' : '')}
          </div>
        </div>
        <div>
          <SelectField
            floatingLabelText="Call From"
            floatingLabelFixed
            fullWidth
            value={callFromNumber}
            onChange={selectCallFromNumber}
            floatingLabelStyle={{
              fontSize: 20,
              color: 'rgba(0, 0, 0, .6)',
              fontWeight: 500,
            }}
          >
            {localNumbers && localNumbers.map(localNumber =>
              <MenuItem
                key={localNumber.id}
                value={localNumber.number}
                primaryText={localNumberToText(localNumber)}
              />
            )}
          </SelectField>
        </div>
      </div>

      <div>
        <div>
          Last Contacted
        </div>
        <div>
          <div style={{ fontSize: 24, fontWeight: 'normal' }}>
            {newCall.lastContacted ? newCall.lastContacted + ' ago' : 'Never'}
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <RaisedButton
            label="Purchase new number"
            onTouchTap={() => purchaseNumber(newCall.areaCode, newCall.country, newCall.to)}
            disabled={!!localNumber}
            primary
          />
        </div>
      </div>

  
      
    </CallInfo>
    <Form
      id="call-modal-form"
      model="call"
      onSubmit={hangUp}
      style={{ width: '100%' }}
    >
      <InputText
        model=".notes"
        inputStyle={{ fontWeight: 300, fontSize: 13 }}
        floatingLabelText="Note"
        rows={4}
        multiLine
        fullWidth
        floatingLabelFixed
      />
    </Form>
  </Dialog>;
};

const ModalTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .number {
    font-weight: 300;
    display: inline-block;
    margin-left: 15px;
    @media (max-width: 991px) {
        display:block !important
    }
  }
  .call-timer {
    text-align: right;
    color: #a1a1a1;
    font-size: 13px;
    font-weight: 300;
    .timer {
      font-size: 23px;
      color: #555759;
    }
  }
  
`;

const CallInfo = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 24px;
  > div {
    padding-bottom: 8px;
  }
  @media (max-width: 991px) {
    grid-template-columns:1fr;
    grid-row-gap: 24px;
    text-align:center
  }
  .recordcallblock{
    margin-top:26p;
    @media (max-width: 991px) {
      width:150px;
      margin:26px auto
    }
  }
`;

const WhiteCall = styled(VectorIcon)`
  path {
    fill: #fff;
  }
`;

CallModal.propTypes = {
  newCall: PropTypes.object,
  makeACall: PropTypes.func.isRequired,
  hangUp: PropTypes.func.isRequired,
  callTimer: PropTypes.number.isRequired,
  toggleRecordCall: PropTypes.func.isRequired,
  recordCall: PropTypes.bool.isRequired,
  selectCallFromNumber: PropTypes.func.isRequired,
  callFromNumber: PropTypes.string,
  callStatus: PropTypes.bool.isRequired,
  localNumbers: PropTypes.array.isRequired,
  sharedLocalNumber: PropTypes.string,
  localNumber: PropTypes.string,
  currentUserId: PropTypes.number.isRequired,
  purchaseNumber: PropTypes.func.isRequired,
};

export default smartify(CallModal);
