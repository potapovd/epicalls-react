import { compose } from 'redux';
import { actions } from 'react-redux-form';
import { connect } from 'react-redux';
import { createSelector, createStructuredSelector } from 'reselect';
import standardErrorHandling from 'utils/standard-error-handling';
import api from 'api';
import { withState, withHandlers } from 'recompose';

const clientNumberError = createSelector(
  state => state.forms.call.$form.validity.error,
  state => state.forms.call.$form.submitFailed,
  state => state.forms.call.number.errors,
  (serverError, submitFailed, { correctNumber, hasCountryCode }) => {
    if (serverError) return serverError;
    if (submitFailed && correctNumber) return 'Number must contains only digits';
    if (submitFailed && hasCountryCode) return 'Please select country code';
  },
);

const selector = createStructuredSelector({
  clientNumberError,
  clientNumber: state => state.call.number,
  twilioProcessLog: state => state.twilioProcessLog,
  callTo: state => state.callTo,
});

const mapDispatchToProps = (dispatch, props) => ({
  submit: model => {
    dispatch(actions.reset('call.commonErrors'));
    dispatch(actions.reset('contact.commonErrors'));
    const request = dispatch(api.actions.callTo.get(model.number)).catch(standardErrorHandling);
    dispatch(actions.submit('call', request, { fields: true }));
  },
  purchaseNumber: () => {
    dispatch(api.actions.addLocalNumber({ localNumber: props.purchaseInfo })).then(_ => {
      props.togglePurchaseModal();
      // Trying to call
      dispatch(actions.submit('call'));
    });
  },
  setCountryCode: clientNumber => countryCode => {
    const cn = clientNumber.replace(/^\+/, '');
    const cc = '+' + countryCode;
    dispatch(actions.change('call.number', cc + cn));
  },
  setCountryCodeFromNumber: e => {
    const match = /^\+(1|44|61|353|57)/.exec(e.target.value);
    if (match) dispatch(actions.change('call.countryCode', +match[1]));
  },
  resetCallForm: () => dispatch(actions.reset('call')),
  changeCallForm: model => dispatch(actions.change('call', model)),
  addContact: number => model => {
    dispatch(actions.reset('call.commonErrors'));
    dispatch(actions.reset('contact.commonErrors'));
    const name = model.name;
    const request = dispatch(api.actions.addContact({ contact: { number, name } }))
      .then(data => {
        props.setMyNewContact(data);
        dispatch(api.actions.currentUser.get());
      })
      .catch(standardErrorHandling);
    dispatch(actions.submit('contact', request, { fields: true }));
  },
});

export default compose(
  withState('callModalOpen', 'openCallModal', false),
  withState('purchaseModalOpen', 'openPurchaseModal', false),
  withState('callInfo', 'setCallInfo', {}),
  withState('purchaseInfo', 'setPurchaseInfo', {}),
  withState('myNewContact', 'setMyNewContact', null),
  withState('dialPadOpen', 'setDialPad', false),
  withHandlers({
    toggleCallModal: ({ openCallModal }) => (e) => openCallModal(current => !current),
    togglePurchaseModal: ({ openPurchaseModal }) => (e) => openPurchaseModal(current => !current),
    closeDialPad: ({ setDialPad }) => e => setDialPad(false),
    openDialPad: ({ setDialPad }) => e => setDialPad(true),
  }),
  connect(selector, mapDispatchToProps),
);
