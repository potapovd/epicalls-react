import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import { Form } from 'react-redux-form';
import RaisedButton from 'material-ui/FlatButton';
import smartify from './smartify';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
  TableFooter,
} from 'material-ui/Table';
import TableHeader from 'material-ui/Table/TableHeader';

const ContactImporter = ({
  open,
  onRequestClose,
  contacts,
  submit,
  toggle,
  rename,
  changePhone,
  ...rest
}) =>
  <Dialog
    open={open}
    onRequestClose={onRequestClose}
    bodyStyle={{ padding: 0 }}
    {...rest}
  >
    <Form
      model="importContacts"
      onSubmit={submit}
    >
      <Table fixedHeader fixedFooter bodyStyle={{ height: 550 }}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn style={{ width: 30 }}>Add</TableHeaderColumn>
            <TableHeaderColumn style={{ textAlign: 'left' }}>Name</TableHeaderColumn>
            <TableHeaderColumn style={{ textAlign: 'left' }}>Phone</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false} showRowHover stripedRows>
          {Object.keys(contacts).map(phone =>
            <TableRow
              key={phone}
              id={phone}
              displayBorder={false}
              selectable={false}
            >
              <TableRowColumn style={{ width: 30 }}>
                <Checkbox
                  id={`${phone}-checkbox`}
                  onCheck={toggle}
                  checked={contacts[phone].checked}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  id={`${phone}-name`}
                  value={contacts[phone].name}
                  onChange={rename}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  id={`${phone}-phone`}
                  value={contacts[phone].number}
                  onChange={changePhone}
                />
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
        <TableFooter adjustForCheckbox={false}>
          <TableRow>
            <TableRowColumn colSpan={3} style={{ textAlign: 'center' }}>
              <RaisedButton type="submit" label="import new contacts" primary />
            </TableRowColumn>
          </TableRow>
        </TableFooter>
      </Table>
    </Form>
  </Dialog>;

ContactImporter.propTypes = {
  open: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  contacts: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
  rename: PropTypes.func.isRequired,
  changePhone: PropTypes.func.isRequired,
};

export default smartify(ContactImporter);
