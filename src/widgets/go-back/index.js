import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import VectorIcon from 'vector-icon';
import FlatButton from 'material-ui/FlatButton';

const GoBack = ({ goBack }) => <FlatButton
  label="Go Back"
  icon={<VectorIcon name="chevronLeft" />}
  onTouchTap={goBack}
  labelStyle={{ textTransform: 'none', fontSize: 15 }}
/>;

GoBack.propTypes = {
  goBack: PropTypes.func.isRequired,
};

export default smartify(GoBack);
