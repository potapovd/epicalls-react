import connectForm from 'utils/connect-form';
import { compose } from 'redux';
import { actions } from 'react-redux-form';

const MODEL = 'manager';

export default compose(
  connectForm({
    form: MODEL,
    action: 'createManager',
    after: (dispatch) => dispatch(actions.reset(MODEL)),
  }),
);
