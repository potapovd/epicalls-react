import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import RoundedRaisedButton from 'components/rounded-raised-button';

const AddAManager = ({
  submit,
}) => <div className="addWidget">
  <h2><span className="bolder">Add</span> A Manager</h2>
  <Form
    model="manager"
    onSubmit={submit}
  >
    <InputText
      model=".manager.firstName"
      hintText="Full Name"
      fullWidth
      required
    />
    <br />
    <InputText
      model=".manager.email"
      hintText="Email"
      fullWidth
      required
    />
    <br />
    <InputText
      model=".manager.companyAttributes.name"
      hintText="Company Name"
      fullWidth
      required
    />
    <br />
    <InputText
      model=".manager.companyAttributes.url"
      hintText="Company Url"
      fullWidth
      required
    />
    <div>
      <ErrorBox model="manager.commonErrors" show />
    </div>
    <div style={{ height: 30 }} />
    <RoundedRaisedButton
      type="submit"
      label="Send Invite"
      fullWidth
      primary
    />
  </Form>
</div>;

AddAManager.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default smartify(AddAManager);
