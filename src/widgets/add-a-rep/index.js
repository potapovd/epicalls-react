import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import VectorIcon from 'vector-icon';
import RoundedRaisedButton from 'components/rounded-raised-button';

const AddARep = ({
  submit,
}) => <div className="addWidget">
  <h2><span className="bolder">Add</span> A Rep</h2>
  <Form
    model="rep"
    onSubmit={submit}
  >
    <InputText
      model=".rep.firstName"
      hintText={<span><UserIcon name="user" style={{ position: 'relative', marginRight: 8, marginLeft: 4 }} />Full Name</span>}
      fullWidth
      required
    />
    <br />
    <InputText
      model=".rep.email"
      hintText={<span><VectorIcon name="envelope" style={{ position: 'relative', marginRight: 8, marginLeft: 4 }} />Email</span>}
      fullWidth
      required
    />
    <br />
    <InputText
      model=".rep.forwardingNumberAttributes.number"
      hintText={<span><VectorIcon name="phone" style={{ position: 'relative', marginRight: 8, marginLeft: 4 }} />Forwarding number</span>}
      fullWidth
      required
    />
    <div>
      <ErrorBox model="rep.commonErrors" show />
    </div>
    <div style={{ height: 30 }} />
    <RoundedRaisedButton
      type="submit"
      label="Send Invite"
      fullWidth
      primary
    />
  </Form>
</div>;

const UserIcon = styled(VectorIcon)`
  > path {
    fill: #000;
    opacity: .4;
  }
`;

AddARep.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default smartify(AddARep);
