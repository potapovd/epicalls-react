import connectForm from 'utils/connect-form';
import { compose } from 'redux';
import { actions } from 'react-redux-form';

const MODEL = 'rep';

export default compose(
  connectForm({
    form: MODEL,
    action: 'createRep',
    after: (dispatch) => dispatch(actions.reset(MODEL)),
  }),
);
