import { createForms } from 'react-redux-form';

export default createForms({
  search: {
    query: '',
  },
  searchManagers: {
    query: '',
  },
  searchAdmins: {
    query: '',
  },
  searchContacts: {
    query: '',
  },
  searchCallLogs: {
    query: '',
  },
  searchManagerCallLogs: {
    query: '',
  },
  searchReps: {
    query: '',
  },
  contact: {
    name: '',
  },
  validationCode: {
    code: '',
  },
  forwardingNumber: {
    number: '',
  },
  call: {
    countryCode: '',
    number: '',
    notes: '',
  },
  login: {
    auth: {
      email: '',
      password: '',
    },
  },
  signup: {
    user: {
      firstName: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      companyAttributes: {
        name: '',
        url: '',
      },
    },
  },
  rep: {
    rep: {
      firstName: '',
      email: '',
      forwardingNumberAttributes: {
        number: '',
      },
    },
  },
  manager: {
    manager: {
      firstName: '',
      email: '',
      companyAttributes: {
        name: '',
        url: '',
      },
    },
  },
  admin: {
    admin: {
      firstName: '',
      email: '',
    },
  },
  passwordReset: {
    email: '',
    id: '',
    user: {
      password: '',
      passwordConfirmation: '',
    },
  },
  resetPassword: {
    email: '',
  },
  changeUserFirstName: {
    user: { firstName: '' },
  },
  changeUserEmail: {
    id: '',
  },
  removeUserEmail: {
    id: '',
  },
  changeUserPassword: {
    user: {
      oldPassword: '',
      password: '',
      passwordConfirmation: '',
    },
  },
  addUserEmail: {
    email: { email: '' },
  },
  webhook: {
    webhook: {
      link: '',
      webhookActionId: '',
    },
  },
  testWebhook: {
    body: '{"action": "item changed", "data": "action details"}',
  },
  importContacts: {
    contacts: {},
    commonErrors: {},
  },
});
