import { compose } from 'redux';
import { withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import { createStructuredSelector } from 'reselect';
import { calls as callsSelector, searchQuery } from 'selectors';

const query = searchQuery('searchCallLogs');
const userCalls = state => state.currentUser.calls;

const calls = callsSelector(query, userCalls);

const selector = createStructuredSelector({ calls });

const mapDispatchToProps = (dispatch, props) => ({
  callTo: contact => (e) => {
    e.stopPropagation();
    dispatch(actions.change('call.number', contact.number));
    dispatch(actions.submit('call'));
  },
});

export default compose(
  withState('callInfoModalOpen', 'openCallInfoModal', false),
  withState('callInfo', 'setCallInfo', {}),
  withHandlers({
    toggleCallInfoModal: ({ openCallInfoModal }) => (e) => openCallInfoModal(current => !current),
  }),
  connect(selector, mapDispatchToProps),
);
