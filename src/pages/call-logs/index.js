import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import CallInfoModal from './call-info-modal';
import MakeACall from 'widgets/make-a-call';
import VectorIcon from 'vector-icon';
import { Link } from 'react-router-dom';
import moment from 'moment';
import SearchForm from 'components/search-form';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton/FlatButton';

const iconInfo = require('assets/images/icon-info.png');

const CallLogs = ({
  currentUser,
  callInfoModalOpen,
  toggleCallInfoModal,
  callInfo,
  setCallInfo,
  callTo,
  calls,
  match,
}) => {
  function numberOrName(entity) {
    if (entity.contact) return entity.contact.name;
    return entity.number;
  }
  return <ManagerDashboard>
    <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
      <SubMenu role={currentUser.role} path={match.path} />
      <SearchForm model="searchCallLogs" />
    </div>
    <div />
    <div>
      <div className="shadow">
        <Table
          className="call-logs-table"
          height="calc(100vh - 229px)"
          bodyStyle={{ minWidth: 870 }}
          wrapperStyle={{ minWidth: 870 }}
          fixedHeader
        >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn style={{ width: 46, paddingLeft: 6, paddingRight: 0 }} />
              <TableHeaderColumn style={{ paddingLeft: 8 }}>From</TableHeaderColumn>
              <TableHeaderColumn>To</TableHeaderColumn>
              <TableHeaderColumn>Start Time</TableHeaderColumn>
              <TableHeaderColumn>Duration, s</TableHeaderColumn>
              <TableHeaderColumn>Direction</TableHeaderColumn>
              <TableHeaderColumn />
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover stripedRows>
            {calls.map(entity =>
              <TableRow
                key={entity.id}
                onTouchTap={() => {
                  setCallInfo(entity);
                  toggleCallInfoModal();
                }}
                displayBorder={false}
              >
                <TableRowColumn
                  style={{ width: 46, paddingLeft: 6, paddingRight: 0 }}
                  onTouchTap={e => e.stopPropagation()}
                >
                  <VectorIcon
                    name="handsetOnGreen"
                    className="handset-on-green"
                    onClick={callTo(entity)}
                  />
                </TableRowColumn>
                <TableRowColumn style={{ paddingLeft: 8 }}>
                  {entity.incoming ? numberOrName(entity) : entity.localNumber}
                </TableRowColumn>
                <TableRowColumn>
                  {entity.incoming ? currentUser.number : numberOrName(entity)}
                </TableRowColumn>
                <TableRowColumn>
                  {(new Date(entity.createdAt)).toLocaleString()}
                </TableRowColumn>
                <TableRowColumn>
                  {moment.duration(entity.duration * 1000).humanize()}
                </TableRowColumn>
                <TableRowColumn>
                  {entity.incoming ? 'Incoming' : 'Outgoing Dial'}
                </TableRowColumn>
                <TableRowColumn style={{ textAlign: 'center' }}>
                  <img src={iconInfo} />
                </TableRowColumn>
              </TableRow>
            )}
            {currentUser.calls.length === 0 && <TableRow>
              <TableRowColumn style={{ textAlign: 'center' }}>Get started by making a phone call</TableRowColumn>
            </TableRow>}
          </TableBody>
        </Table>
      </div>
      <CallInfoModal {...{ callInfoModalOpen, toggleCallInfoModal, callInfo, callTo }} />
    </div>
    {currentUser.forwardingNumber && currentUser.forwardingNumber.confirmed
      ? <MakeACall />
      : <FlatButton
        containerElement={<Link to="/my-numbers" />}
        label="Confirm Outbound Number"
        fullWidth
        secondary
      />
    }
  </ManagerDashboard>;
};

const ManagerDashboard = styled.div`
  display: grid;
  grid-template-columns: 1fr 288px;
  grid-column-gap: 56px;
`;

CallLogs.propTypes = {
  currentUser: PropTypes.object.isRequired,
  callInfoModalOpen: PropTypes.bool.isRequired,
  toggleCallInfoModal: PropTypes.func.isRequired,
  callInfo: PropTypes.object.isRequired,
  setCallInfo: PropTypes.func.isRequired,
  callTo: PropTypes.func.isRequired,
  calls: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(CallLogs);
