import connectForm from 'utils/connect-form';
import { compose } from 'redux';
import { actions } from 'react-redux-form';
import { withRouter } from 'react-router';

export default compose(
  withRouter,
  connectForm({
    form: 'signup',
    action: 'signup',
    after: (dispatch, props) => {
      dispatch(actions.reset('signup'));
      props.history.push('/inactive');
    },
  })
);
