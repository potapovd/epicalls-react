import { compose } from 'redux';
import { connect } from 'react-redux';
import api from 'api';
import { createStructuredSelector } from 'reselect';
import withJob from 'utils/with-job';
import { toggleAddOn } from 'actions';

const selector = createStructuredSelector({
  addOns: state => state.addOns,
});

const mapDispatchToProps = (dispatch, props) => ({
  load: () => {
    dispatch(api.actions.addOns.get());
  },
  toggle: (sid, installed) => () => {
    if (installed) {
      dispatch(api.actions.removeAddOn(sid));
    } else {
      dispatch(api.actions.createAddOn({ addOn: { sid } }));
    }
    dispatch(toggleAddOn({ sid }));
  },
});

const work = ({ load }) => load();

export default compose(
  connect(selector, mapDispatchToProps),
  withJob({ work }),
);
