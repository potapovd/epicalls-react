import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import Toggle from 'material-ui/Toggle';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const AddOns = ({
  currentUser,
  addOns,
  toggle,
  match,
}) => {
  return <div>
    <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
      <SubMenu role={currentUser.role} path={match.path} />
    </div>
    <div>
      <div>
        <div className="shadow">
          <Table
            className="add-ons-table"
            height="calc(100vh - 229px)"
            bodyStyle={{ minWidth: 870 }}
            wrapperStyle={{ minWidth: 870 }}
            fixedHeader
          >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn style={{ width: '30%' }}>Name</TableHeaderColumn>
                <TableHeaderColumn style={{ width: '50%' }}>Description</TableHeaderColumn>
                <TableHeaderColumn style={{ width: '20%' }} />
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} showRowHover stripedRows>
              {addOns.map(entity =>
                <TableRow
                  key={entity.sid}
                  displayBorder={false}
                >
                  <TableRowColumn style={{ width: '30%' }}>{entity.friendlyName}</TableRowColumn>
                  <TableRowColumn style={{ width: '50%' }}>{entity.description}</TableRowColumn>
                  <TableRowColumn style={{ width: '20%' }} >
                    <Toggle
                      toggled={entity.installed}
                      onToggle={toggle(entity.sid, entity.installed)}
                    />
                  </TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      </div>
    </div>
  </div>;
};

AddOns.propTypes = {
  currentUser: PropTypes.object.isRequired,
  addOns: PropTypes.array.isRequired,
  toggle: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(AddOns);
