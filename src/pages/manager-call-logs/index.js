import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import CallInfoModal from './call-info-modal';
import moment from 'moment';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SearchForm from 'components/search-form';

const iconInfo = require('assets/images/icon-info.png');

const CallLogs = ({
  currentUser,
  callInfoModalOpen,
  toggleCallInfoModal,
  callInfo,
  setCallInfo,
  callTo,
  calls,
  match,
}) => {
  function numberOrName(entity) {
    if (entity.contact) return entity.contact.name;
    return entity.number;
  }
  return <div>
    <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
      <SubMenu role={currentUser.role} path={match.path} />
      <SearchForm model="searchManagerCallLogs" />
    </div>
    <div>
      <div>
        <div className="shadow">
          <Table
            className="manager-call-logs-table"
            height="calc(100vh - 229px)"
            bodyStyle={{ minWidth: 870 }}
            wrapperStyle={{ minWidth: 870 }}
            fixedHeader
          >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>From</TableHeaderColumn>
                <TableHeaderColumn>To</TableHeaderColumn>
                <TableHeaderColumn>Start Time</TableHeaderColumn>
                <TableHeaderColumn>Duration</TableHeaderColumn>
                <TableHeaderColumn>Direction</TableHeaderColumn>
                <TableHeaderColumn />
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} showRowHover stripedRows>
              {calls.map(entity =>
                <TableRow
                  key={entity.id}
                  onTouchTap={() => {
                    setCallInfo(entity);
                    toggleCallInfoModal();
                  }}
                  displayBorder={false}
                >
                  <TableRowColumn>
                    {entity.incoming ? entity.number : entity.userName}
                  </TableRowColumn>
                  <TableRowColumn>
                    {entity.incoming ? entity.userName : entity.number}
                  </TableRowColumn>
                  <TableRowColumn>
                    {(new Date(entity.createdAt)).toLocaleString()}
                  </TableRowColumn>
                  <TableRowColumn>
                    {moment.duration(entity.duration  * 1000).humanize()}
                  </TableRowColumn>
                  <TableRowColumn>
                    {entity.incoming ? 'Incoming' : 'Outgoing Dial'}
                  </TableRowColumn>
                  <TableRowColumn style={{ textAlign: 'center' }}>
                    <img src={iconInfo} />
                  </TableRowColumn>
                </TableRow>
              )}
              {currentUser.calls.length === 0 && <TableRow>
                <TableRowColumn style={{ textAlign: 'center' }}>You have no calls to display.</TableRowColumn>
              </TableRow>}
            </TableBody>
          </Table>
        </div>
        <CallInfoModal {...{ callInfoModalOpen, toggleCallInfoModal, callInfo, numberOrName }} />
      </div>
    </div>
  </div>;
};

CallLogs.propTypes = {
  currentUser: PropTypes.object.isRequired,
  callInfoModalOpen: PropTypes.bool.isRequired,
  toggleCallInfoModal: PropTypes.func.isRequired,
  callInfo: PropTypes.object.isRequired,
  setCallInfo: PropTypes.func.isRequired,
  callTo: PropTypes.func.isRequired,
  calls: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(CallLogs);
