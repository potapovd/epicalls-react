import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import IconButton from 'material-ui/IconButton';
import AlertBox from 'components/alert-box';
import VectorIcon from 'vector-icon';
import AddARep from 'widgets/add-a-rep';
import Toggle from 'material-ui/Toggle';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SearchForm from 'components/search-form';

const Reps = ({
  currentUser,
  removeSalesRep,
  salesRepToRemove,
  setSalesRepToRemove,
  removeSalesRepModalOpen,
  toggleRemoveSalesRepModal,
  reps,
  loginAsRep,
  toggleRep,
  toggledRep,
  setToggledRep,
  removeRepLocalNumbers,
  match,
}) =>
  <ManagerDashboard>
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
        <SubMenu role={currentUser.role} path={match.path} />
        <SearchForm model="searchReps" />
      </div>
        <div className="shadow">
          <Table
            className="sales-reps-table responsive-table"
            fixedHeader
          >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>Email</TableHeaderColumn>
                <TableHeaderColumn>Forwarding Number</TableHeaderColumn>
                <TableHeaderColumn>Local Numbers</TableHeaderColumn>
                <TableHeaderColumn>Active</TableHeaderColumn>
                <TableHeaderColumn>Login as Rep</TableHeaderColumn>
                <TableHeaderColumn />
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} stripedRows>
              {reps.map(entity =>
                <TableRow
                  key={entity.id}
                  displayBorder={false}
                  selectable={false}
                >
                  <TableRowColumn>
                    <span className="responsive-table-subname">Name</span>
                    {entity.firstName}
                  </TableRowColumn>
                  <TableRowColumn>
                    <span className="responsive-table-subname">Email</span>
                    {entity.email}
                  </TableRowColumn>
                  <TableRowColumn>
                    <span className="responsive-table-subname">Forwarding Number</span>
                    {entity.number}
                  </TableRowColumn>
                  <TableRowColumn>
                    <span className="responsive-table-subname">Local Numbers</span>
                    {entity.localNumbersCount}
                  </TableRowColumn>
                  <TableRowColumn>
                    <span className="responsive-table-subname">Active</span>
                    <Toggle
                      toggled={entity.active}
                      onToggle={(e, isInputChecked) => {
                        if (!isInputChecked && entity.localNumbersCount > 0) {
                          setToggledRep(entity);
                        }
                        toggleRep(entity);
                      }}
                    />
                  </TableRowColumn>
                  <TableRowColumn style={{ textAlign: 'right' }}>
                    <span className="responsive-table-subname">Login as Rep</span>
                    <IconButton onClick={() => {
                      loginAsRep(entity.id);
                    }}>
                      <UserIcon name="user" />
                    </IconButton>
                  </TableRowColumn>
                  <TableRowColumn style={{ textAlign: 'right' }}>
                    <IconButton onClick={() => {
                      setSalesRepToRemove(entity);
                      toggleRemoveSalesRepModal();
                    }}>
                      <VectorIcon name="trash" hovered="trashed" />
                    </IconButton>
                  </TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>
          <AlertBox
            open={removeSalesRepModalOpen}
            yesAction={removeSalesRep}
            onRequestClose={toggleRemoveSalesRepModal}
          >
            Are you sure you want to delete
            <div style={{
              fontSize: 20,
              fontWeight: 500,
              lineHeight: 1.35,
            }}>
              {salesRepToRemove.name}?
            </div>
          </AlertBox>
          <AlertBox
            open={!!toggledRep}
            yesAction={removeRepLocalNumbers(toggledRep)}
            onRequestClose={() => setToggledRep(null)}
          >
            Do you want to release <b>{toggledRep && toggledRep.localNumbersCount}</b> Twilio numbers<br />
            purchased by <b>{toggledRep && toggledRep.name}</b>?
          </AlertBox>
        </div>
    </div>
    <AddARep />
  </ManagerDashboard>;

const ManagerDashboard = styled.div`
  display: grid;
  grid-template-columns:minmax(300px,1fr) 288px;
  grid-column-gap: 56px;
  @media (max-width: 991px) {
    grid-template-columns:1fr;
  }
`;

const UserIcon = styled(VectorIcon)`
  > path {
    fill: #000;
    opacity: .4;
  }
`;

Reps.propTypes = {
  currentUser: PropTypes.object.isRequired,
  removeSalesRep: PropTypes.func.isRequired,
  salesRepToRemove: PropTypes.object,
  setSalesRepToRemove: PropTypes.func.isRequired,
  removeSalesRepModalOpen: PropTypes.bool.isRequired,
  toggleRemoveSalesRepModal: PropTypes.func.isRequired,
  reps: PropTypes.array.isRequired,
  loginAsRep: PropTypes.func.isRequired,
  toggleRep: PropTypes.func.isRequired,
  toggledRep: PropTypes.any,
  setToggledRep: PropTypes.func.isRequired,
  removeRepLocalNumbers: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(Reps);
