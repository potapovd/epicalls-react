import React from 'react';
import PropTypes from 'prop-types';
import SubMenu from 'components/sub-menu';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import InputSelect from 'components/input-select';
import MenuItem from 'material-ui/MenuItem';
import smartify from './smartify';
import ErrorBox from 'components/error-box';
import IconButton from 'material-ui/IconButton';
import VectorIcon from 'vector-icon';
import AlertBox from 'components/alert-box';
import InfoBox from 'components/info-box';
import DialogBoxButton from 'components/dialog-box/button';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

const Webhooks = ({
  currentUser,
  match,
  submit,
  webhookActions,
  webhookToRemove,
  setWebhookToRemove,
  removeWebhook,
  webhookToTest,
  setWebhookToTest,
  testWebhook,
  closeTestWebhook,
}) => <div>
  <SubMenu role={currentUser.role} path={match.path} />
  <div className="shadow" style={{ height: 'calc(100vh - 179px)' }}>
    <Form
      model="webhook"
      onSubmit={submit}
    >
      <Table
        className="webhooks-table"
        height="calc(100vh - 229px)"
        bodyStyle={{ minWidth: 870 }}
        wrapperStyle={{ minWidth: 870 }}
        fixedHeader
      >
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>Url</TableHeaderColumn>
            <TableHeaderColumn style={{ width: 200 }}>Action</TableHeaderColumn>
            <TableHeaderColumn style={{ width: 100 }} />
          </TableRow>
          <TableRow>
            <TableHeaderColumn colSpan={3}>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ flexGrow: 1, paddingRight: 24 }}>
                  <InputText
                    model=".webhook.link"
                    hintText="Valid URL"
                    required
                    fullWidth
                  />
                </div>
                <div style={{ width: 200, padding: '0 24px', position: 'relative', bottom: -5 }}>
                  <InputSelect
                    model=".webhook.webhookActionId"
                    fullWidth
                    required
                  >
                    {webhookActions.map(a =>
                      <MenuItem
                        key={a.id}
                        value={a.id}
                        primaryText={a.name}
                      />
                    )}
                  </InputSelect>
                </div>
                <div style={{ width: 100, paddingLeft: 24 }}>
                  <FlatButton
                    type="submit"
                    label="Add"
                    primary
                  />
                </div>
              </div>
              <ErrorBox model="webhook.commonErrors" show />
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false} showRowHover stripedRows>
          {currentUser.webhooks.map(webhook =>
            <TableRow
              key={webhook.id}
              displayBorder={false}
              onTouchTap={(e) => {
                e.stopPropagation();
                setWebhookToTest(webhook);
              }}
            >
              <TableRowColumn>
                {webhook.link}
              </TableRowColumn>
              <TableRowColumn style={{ width: 200 }}>
                {webhook.name}
              </TableRowColumn>
              <TableRowColumn style={{ width: 100, textAlign: 'center' }}>
                <IconButton onTouchTap={(e) => {
                  e.stopPropagation();
                  setWebhookToRemove(webhook);
                }}>
                  <VectorIcon name="trash" hovered="trashed" />
                </IconButton>
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </Form>
  </div>
  <AlertBox
    open={!!webhookToRemove}
    yesAction={removeWebhook}
    onRequestClose={closeTestWebhook}
  >
    Are you sure you want to remove webhook?
  </AlertBox>
  {webhookToTest && <InfoBox
    open={!!webhookToTest}
    onRequestClose={closeTestWebhook}
    actions={[
      <DialogBoxButton
        key="cancel"
        label="cancel"
        onTouchTap={closeTestWebhook}
        style={{ borderRight: '1px solid #eee' }}
      />,
      <DialogBoxButton
        key="send"
        label="send"
        form="test-webhook-form"
        type="submit"
      />,
    ]}
  >
    <h2>Send test data</h2>
    <Form
      model="testWebhook"
      onSubmit={testWebhook}
      id="test-webhook-form"
    >
      <InputText
        model=".body"
        hintText="JSON string"
        rows={3}
        multiLine
        fullWidth
        required
      />
      <ErrorBox model="testWebhook.commonErrors" show />
    </Form>
    <p>
      Webhook URL: <b>{webhookToTest.link}</b>
    </p>
  </InfoBox>}
</div>;

Webhooks.propTypes = {
  currentUser: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  webhookActions: PropTypes.array.isRequired,
  webhookToRemove: PropTypes.any,
  setWebhookToRemove: PropTypes.func.isRequired,
  removeWebhook: PropTypes.func.isRequired,
  webhookToTest: PropTypes.any,
  setWebhookToTest: PropTypes.func.isRequired,
  testWebhook: PropTypes.func.isRequired,
  closeTestWebhook: PropTypes.func.isRequired,
};

export default smartify(Webhooks);
