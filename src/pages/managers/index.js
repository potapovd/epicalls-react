import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import IconButton from 'material-ui/IconButton';
import AlertBox from 'components/alert-box';
import VectorIcon from 'vector-icon';
import AddAManager from 'widgets/add-a-manager';
import Toggle from 'material-ui/Toggle';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SearchForm from 'components/search-form';

const Managers = ({
  currentUser,
  removeManager,
  managerToRemove,
  setManagerToRemove,
  managers,
  loginAsManager,
  toggleManager,
  match,
}) =>
  <ManagerDashboard>
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
        <SubMenu role={currentUser.role} path={match.path} />
        <SearchForm model="searchManagers" />
      </div>
      <div className="shadow">
        <Table
          className="managers-table responsive-table"
          fixedHeader
        >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>Company Name</TableHeaderColumn>
              <TableHeaderColumn>Manager Name</TableHeaderColumn>
              <TableHeaderColumn>Manager Email</TableHeaderColumn>
              <TableHeaderColumn>Reps</TableHeaderColumn>
              <TableHeaderColumn>Active</TableHeaderColumn>
              <TableHeaderColumn>Login as Manager</TableHeaderColumn>
              <TableHeaderColumn />
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} stripedRows>
            {managers.map(entity =>
              <TableRow
                key={entity.id}
                displayBorder={false}
                selectable={false}
              >
                <TableRowColumn>
                  <span className="responsive-table-subname">Company Name</span>
                    {entity.company.name}
                </TableRowColumn>
                <TableRowColumn>
                <span className="responsive-table-subname">Manager Name</span>
                  {entity.name}
                </TableRowColumn>
                <TableRowColumn>
                <span className="responsive-table-subname">Manager Email</span>
                  {entity.email}
                </TableRowColumn>
                <TableRowColumn>
                <span className="responsive-table-subname">Reps</span>
                  {entity.repsCount}
                </TableRowColumn>
                <TableRowColumn>
                  <span className="responsive-table-subname">Active</span>
                  <Toggle 
                    className="responsive-table-toggler"
                    toggled={entity.active}
                    onToggle={toggleManager(entity)}
                  />
                </TableRowColumn>
                <TableRowColumn style={{ textAlign: 'right' }}>
                  <span className="responsive-table-subname">Login as Manager</span>
                  <IconButton onClick={() => {
                    loginAsManager(entity.id);
                  }}>
                    <UserIcon name="user" />
                  </IconButton>
                </TableRowColumn>
                <TableRowColumn style={{ textAlign: 'right' }}>
                  <span className="responsive-table-subname">Remove Manager</span>
                  <IconButton onClick={() => {
                    setManagerToRemove(entity);
                  }}>
                    <VectorIcon name="trash" hovered="trashed" />
                  </IconButton>
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
        <AlertBox
          open={!!managerToRemove}
          yesAction={removeManager}
          onRequestClose={() => setManagerToRemove(null)}
        >
          Are you sure you want to delete
          <div style={{
            fontSize: 20,
            fontWeight: 500,
            lineHeight: 1.35,
          }}>
            {managerToRemove && managerToRemove.name}?
          </div>
        </AlertBox>
      </div>
    </div>
    <AddAManager />
  </ManagerDashboard>;

const ManagerDashboard = styled.div`
  display: grid;
  grid-template-columns:minmax(300px,1fr) 288px;
  grid-column-gap: 56px;
  @media (max-width: 991px) {
    grid-template-columns:1fr;
  }
`;

const UserIcon = styled(VectorIcon)`
  > path {
    fill: #000;
    opacity: .4;
  }
`;

Managers.propTypes = {
  currentUser: PropTypes.object.isRequired,
  removeManager: PropTypes.func.isRequired,
  managerToRemove: PropTypes.object,
  setManagerToRemove: PropTypes.func.isRequired,
  managers: PropTypes.array.isRequired,
  loginAsManager: PropTypes.func.isRequired,
  toggleManager: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(Managers);
