import { createSelector, createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withState } from 'recompose';
import { searchQuery } from 'selectors';
import api from 'api';

const query = searchQuery('searchManagers');
const managers = state => state.currentUser.managers;

const filteredManagers = createSelector(
  query,
  managers,
  (query, managers) => managers.filter(c => {
    if (query !== '') {
      const re = new RegExp(query, 'i');
      return re.test(c.name) || re.test(c.email) || re.test(c.company.name);
    } else return true;
  }),
);

const selector = createStructuredSelector({ managers: filteredManagers });

const mapDispatchToProps = (dispatch, props) => ({
  removeManager: () => {
    dispatch(api.actions.removeManager(props.managerToRemove.id))
      .then(props.setManagerToRemove(null));
  },
  loginAsManager: id => dispatch(api.actions.managerToken(id)),
  toggleManager: manager => () => dispatch(api.actions.updateManager(manager.id, { manager: { active: !manager.active } })),
});

export default compose(
  withState('managerToRemove', 'setManagerToRemove', null),
  connect(selector, mapDispatchToProps),
);
