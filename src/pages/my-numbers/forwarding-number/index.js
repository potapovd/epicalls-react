import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import VectorIcon from 'vector-icon';
import AlertBox from 'components/alert-box';
import smartify from './smartify';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import FlatButton from 'material-ui/FlatButton';
import DialogBoxButton from 'components/dialog-box/button';
import ErrorBox from 'components/error-box';
import Snackbar from 'material-ui/Snackbar';

const ForwardingNumber = ({
  currentUser,
  validationCallModalOpen,
  makeValidationCall,
  checkValidationCode,
  setValidationCallModal,
  validationError,
  setValidationError,
}) => <div>
  <div className="my-numbers-header">
    <div>My Outbound Number</div>
    <div className="my-numbers-header-help">
      Inbound calls to your purchase number will be forwarded to you outbound number
    </div>
  </div>
  <OutboundNumberWrapper>
    <NumberHelpText>
      <VectorIcon name="call" />
      <span>Outbound Number</span>
    </NumberHelpText>
    <OutboundNumber>
      {currentUser.number}
    </OutboundNumber>
    {!currentUser.forwardingNumber.confirmed && <FlatButton
      label="Confirm by phone call"
      onTouchTap={makeValidationCall}
      secondary
    />}
  </OutboundNumberWrapper>
  <AlertBox
    open={validationCallModalOpen}
    onRequestClose={() => setValidationCallModal(false)}
    yesAction={() => false}
    actions={[
      <DialogBoxButton
        key="cancel"
        label="cancel"
        onClick={() => setValidationCallModal(false)}
        hoverColor="#eaeaea"
        style={{borderRight: '1px solid #cecece'}}
      />,
      <DialogBoxButton
        key="send"
        label="send"
        type="submit"
        hoverColor="#eaeaea"
        form="validation-code-form"
      />,
    ]}
  >
    <b>We are calling you now...</b><br />Please provide your code
    <Form id="validation-code-form" model="validationCode" onSubmit={checkValidationCode}>
      <InputText
        model=".code"
        style={{width: 216}}
        required
      />
    </Form>
    <ErrorBox model="validationCode.commonErrors" show />
  </AlertBox>
  {validationError && <Snackbar
    open={!!validationError}
    message={validationError}
    autoHideDuration={10000}
    onRequestClose={() => setValidationError(null)}
    contentStyle={{
      color: 'rgb(255, 64, 129)',
    }}
  />}
</div>;

ForwardingNumber.propTypes = {
  currentUser: PropTypes.object.isRequired,
  validationCallModalOpen: PropTypes.bool.isRequired,
  makeValidationCall: PropTypes.func.isRequired,
  checkValidationCode: PropTypes.func.isRequired,
  setValidationCallModal: PropTypes.func.isRequired,
  validationError: PropTypes.any,
  setValidationError: PropTypes.func.isRequired,
};

const OutboundNumber = styled.span`
  font-size: 20px;
  letter-spacing: 0.8px;
  color: #5cbece;
  display: inline-block;
  margin-right: 16px;
`;

const OutboundNumberWrapper = styled.div`
  padding: 1.5rem 30px;
  border-top: 1px solid #fafafa;
`;

const NumberHelpText = styled.div`
  color: #a1a1a1;
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 1.5;
  margin-bottom: 0.5rem;
  > svg {
    margin-right: 8px;
  }
`;

export default smartify(ForwardingNumber);
