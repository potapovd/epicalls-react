import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PurchasedNumbers from './purchased-numbers';
import ForwardingNumber from './forwarding-number';
import GoBack from 'widgets/go-back';

const MyNumbers = (props) => <div>
  <GoBack />
  <div style={{ height: '2rem', lineHeight: '1' }} />
  <TwoColumns>
    <ForwardingNumber {...props} />
    <PurchasedNumbers {...props} />
  </TwoColumns>
</div>;

const TwoColumns = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 20px;
  > div {
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 0 65px 0 rgba(20, 41, 59, 0.22);
    div.my-numbers-header {
      font-size: 24px;
      color: #555759;
      padding-left: 30px;
      height: 96px;
      display: flex;
      align-items: center;
      div.my-numbers-header-help {
        font-size: 13px;
        font-weight: 300;
        line-height: 1.23;
        color: #737373;
      }
      > div {
        flex-grow: 1;
        width: 50%;
        padding-right: 30px;
      }
    }
  }
`;

MyNumbers.propTypes = {
  currentUser: PropTypes.object,
};

export default MyNumbers;
