import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import VectorIcon from 'vector-icon';
import AlertBox from 'components/alert-box';

const PurchasedNumbers = ({
  currentUser,
  setNumberToRemove,
  toggleRemoveNumberModal,
  numberToRemove,
  removeNumber,
  removeNumberModalOpen,
}) => <div>
  <div className="my-numbers-header">
    <div>My Purchased Numbers</div>
  </div>
  <div style={{ height: 'calc(100vh - 280px)', overflow: 'auto' }}>
    {currentUser.localNumbers.map(localNumber =>
      <NumberWrapper key={localNumber.id}>
        <div style={{ width: '50%' }}>
          <NumberHelpText>
            <VectorIcon name="pin" />
            <span>Location</span>
          </NumberHelpText>
          <NumberLocation>
            {localNumber.geoName + ', ' + localNumber.country}
          </NumberLocation>
        </div>
        <div style={{ width: '40%' }}>
          <NumberHelpText>
            <VectorIcon name="call" />
            <span>Purchased Number</span>
          </NumberHelpText>
          <PurchasedNumber>{localNumber.number}</PurchasedNumber>
        </div>
        <div style={{ width: '10%' }}>
          <VectorIcon name="trash" style={{ cursor: 'pointer' }} onClick={e => {
            setNumberToRemove(localNumber);
            toggleRemoveNumberModal();
          }} />
        </div>
      </NumberWrapper>
    )}
  </div>
  <AlertBox
    open={removeNumberModalOpen}
    onRequestClose={toggleRemoveNumberModal}
    yesAction={removeNumber(numberToRemove.id)}
  >
    Remove number <b>{numberToRemove.number}</b> ?
  </AlertBox>
</div>;

PurchasedNumbers.propTypes = {
  currentUser: PropTypes.object.isRequired,
  setNumberToRemove: PropTypes.func.isRequired,
  toggleRemoveNumberModal: PropTypes.func.isRequired,
  numberToRemove: PropTypes.object.isRequired,
  removeNumber: PropTypes.func.isRequired,
  removeNumberModalOpen: PropTypes.bool.isRequired,
};

const NumberWrapper = styled.div`
  padding: 1.5rem 30px;
  border-top: 1px solid #fafafa;
  display: flex;
  align-items: center;
`;

const NumberLocation = styled.div`
  font-size: 18px;
  color: #555759;
  line-height: 24px;
  padding: .5rem 0;
`;

const NumberHelpText = styled.div`
  color: #a1a1a1;
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 1.5;
  > svg {
    margin-right: 8px;
  }
`;

const PurchasedNumber = styled.div`
  font-size: 20px;
  letter-spacing: 0.8px;
  color: #5cbece;
  line-height: 24px;
  padding: .5rem 0;
`;

export default smartify(PurchasedNumbers);
