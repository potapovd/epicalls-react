import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

const selector = createStructuredSelector({
  token: state => state.token,
});

export default connect(selector);
