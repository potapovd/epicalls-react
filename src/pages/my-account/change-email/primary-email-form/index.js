import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-redux-form';
import FlatButton from 'material-ui/FlatButton';
import InputSelect from 'components/input-select';
import MenuItem from 'material-ui/MenuItem';
import smartify from './smartify';

const ChangeEmail = ({ currentEmailId, emails, submit, form }) => {
  return <Form
    model="changeUserEmail"
    onSubmit={submit}
  >
    <InputSelect
      floatingLabelText="Confirmed Emails"
      model=".id"
      defaultValue={currentEmailId}
    >
      {emails.map(email =>
        <MenuItem
          key={email.id}
          value={email.id}
          primaryText={email.email}
        />
      )}
    </InputSelect>
    <FlatButton
      type="submit"
      label="Change Email"
      style={{ marginLeft: 24, position: 'relative', bottom: 16 }}
      primary
    />
    {form.submitted && <div style={{ color: '#388E3C', marginTop: 4 }}>Email was changed, use it for a new login.</div>}
  </Form>;
};

ChangeEmail.propTypes = {
  emails: PropTypes.array.isRequired,
  submit: PropTypes.func.isRequired,
  currentEmailId: PropTypes.number.isRequired,
  form: PropTypes.object.isRequired,
};

export default smartify(ChangeEmail);
