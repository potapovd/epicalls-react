import React from 'react';
import PropTypes from 'prop-types';
import GoBack from 'widgets/go-back';
import styled from 'styled-components';
import AddEmailForm from './add-email-form';
import smartify from './smartify';

const ChangeEmail = ({ confirmedEmails, currentEmailId, emailsToRemove }) => {
  return <div>
    <GoBack />
    <div style={{ height: '2rem', lineHeight: '1' }} />
    <Wrapper>
      <Header>Update Email</Header>
      <Container>
        <AddEmailForm />
      </Container>
    </Wrapper>
  </div>;
};

const Wrapper = styled.div`
  background-color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 65px 0 rgba(20, 41, 59, 0.22);
  padding-bottom: 46px;
`;

const Header = styled.div`
  font-size: 24px;
  color: #555759;
  padding-left: 75px;
  height: 96px;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #fafafa;
`;

const Container = styled.div`
  padding: 0 10%;
`;

ChangeEmail.propTypes = {
  confirmedEmails: PropTypes.array.isRequired,
  currentEmailId: PropTypes.number.isRequired,
  emailsToRemove: PropTypes.array.isRequired,
};

export default smartify(ChangeEmail);
