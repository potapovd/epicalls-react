import { connect } from 'react-redux';
import { createSelector, createStructuredSelector } from 'reselect';

const emails = state => state.currentUser.emails;
const email = state => state.currentUser.email;

const confirmedEmails = createSelector(
  emails,
  emails => emails.filter(e => e.confirmed),
);

const currentEmailId = createSelector(
  emails,
  email,
  (emails, email) => emails.find(e => e.email === email).id,
);

const emailsToRemove = createSelector(
  emails,
  email,
  (emails, email) => emails.filter(e => e.email !== email),
);

const selector = createStructuredSelector({
  confirmedEmails,
  currentEmailId,
  emailsToRemove,
});

export default connect(selector);
