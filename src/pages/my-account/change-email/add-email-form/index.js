import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import FlatButton from 'material-ui/FlatButton';
import smartify from './smartify';

const AddEmailForm = ({ form, email, submit }) => {
  return <Form model="addUserEmail" onSubmit={submit}>
    <InputText
      model=".email.email"
      floatingLabelText="Email"
    />
    <FlatButton
      type="submit"
      label="Change Email"
      style={{ marginLeft: 24 }}
      primary
    />
    <ErrorBox model="addUserEmail.commonErrors" show />
    {form.submitted && <div style={{ color: '#388E3C', marginTop: 4 }}>Confirmation email was sent to {email}</div>}
  </Form>;
};

AddEmailForm.propTypes = {
  form: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
};

export default smartify(AddEmailForm);
