import React from 'react';
import PropTypes from 'prop-types';
import GoBack from 'widgets/go-back';
import styled from 'styled-components';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import FlatButton from 'material-ui/FlatButton';
import smartify from './smartify';

const ChangeName = ({ currentUser, submit }) => {
  return <div>
    <GoBack />
    <div style={{ height: '2rem', lineHeight: '1' }} />
    <Wrapper>
      <Header>Change Name</Header>
      <Container>
        <Form model="changeUserFirstName" onSubmit={submit}>
          <InputText
            model=".user.firstName"
            floatingLabelText="Name"
            defaultValue={currentUser.firstName}
          />
          <FlatButton
            type="submit"
            label="Update Name"
            style={{ marginLeft: 24 }}
            primary
          />
          <ErrorBox model="changeUserFirstName.commonErrors" show />
        </Form>
      </Container>
    </Wrapper>
  </div>;
};

const Wrapper = styled.div`
  background-color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 65px 0 rgba(20, 41, 59, 0.22);
  padding-bottom: 46px;
`;

const Header = styled.div`
  font-size: 24px;
  color: #555759;
  padding-left: 75px;
  height: 96px;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #fafafa;
`;

const Container = styled.div`
  padding: 0 10%;
`;

ChangeName.propTypes = {
  currentUser: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
};

export default smartify(ChangeName);
