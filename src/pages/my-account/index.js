import React from 'react';
import PropTypes from 'prop-types';
import GoBack from 'widgets/go-back';
import styled from 'styled-components';
import VectorIcon from 'vector-icon';
import { Link } from 'react-router-dom';
import smartify from './smartify';
import TextField from 'material-ui/TextField';

const MyAccount = ({
  currentUser,
  token,
}) => {
  return <div>
    <GoBack />
    <div style={{ height: '2rem', lineHeight: '1' }} />
    <Wrapper>
      <Header>My Account</Header>
      <Container>
        <Content>
          <div>
            <dt>Name</dt>
            <dd><Link to="/my-account/change-name">{currentUser.firstName} <VectorIcon name="edit" /></Link></dd>
          </div>
          <div>
            <dt>Email</dt>
            <dd><Link to="/my-account/change-email">{currentUser.email} <VectorIcon name="edit" /></Link></dd>
          </div>
          <div>
            <dt>Password</dt>
            <dd><Link to="/my-account/change-password">************* <VectorIcon name="edit" /></Link></dd>
          </div>
          <div>
            <dt>Auth Token</dt>
            <dd>
              <TextField
                value={token}
                multiLine={true}
                rows={2}
                rowsMax={4}
                fullWidth={true}
                onClick={e => e.target.select()}
                readonly
              />
            </dd>
          </div>
        </Content>
      </Container>
    </Wrapper>
  </div>;
};

const Wrapper = styled.div`
  background-color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 65px 0 rgba(20, 41, 59, 0.22);
`;

const Header = styled.div`
  font-size: 24px;
  color: #555759;
  padding-left: 75px;
  height: 96px;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #fafafa;
`;

const Container = styled.div`
  padding: 30px 10%;
`;

const Content = styled.dl`
  div {
    padding: 16px 0;
  }
  dt {
    font-weight: 300;
    display: inline-block;
    width: 80px;
  }
  dd {
    display: inline-block;
    a {
      text-decoration: none;
      color: #3c3d3f;
    }
    svg {
      margin-left: 8px;
      position: relative;
      top: 3px;
      visibility: hidden;
    }
    &:hover {
      svg {
        visibility: visible;
      }
    }
  }
  @media only screen and (max-width: 600px) {
    dd {
      display: block;
      margin-left:0;
    }
  }
`;

MyAccount.propTypes = {
  currentUser: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
};

export default smartify(MyAccount);
