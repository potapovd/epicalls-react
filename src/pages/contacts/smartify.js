import { connect } from 'react-redux';
import { createSelector, createStructuredSelector } from 'reselect';
import { actions } from 'react-redux-form';
import api from 'api';
import { compose } from 'redux';
import { withState } from 'recompose';
import { searchQuery } from 'selectors';
import standardErrorHandling from 'utils/standard-error-handling';

const query = searchQuery('searchContacts');
const contacts = state => state.currentUser.contacts;

const filteredContacts = createSelector(
  query,
  contacts,
  (query, contacts) => contacts.filter(c => {
    if (query !== '') {
      const re = new RegExp(query, 'i');
      return re.test(c.number) || re.test(c.name);
    } else return true;
  }),
);

const contactsByAlphabet = createSelector(
  filteredContacts,
  contacts => contacts.reduce((arr, item) => {
    const firstLetter = item.name.charAt(0).toLowerCase();
    arr[firstLetter] = arr[firstLetter] || [];
    arr[firstLetter].push(item);
    return arr;
  }, {})
);

const selector = createStructuredSelector({
  contactsParserError: state => {
    const error = state.forms.importContacts.commonErrors.$form.errors;
    if (typeof error === 'string') return error;
    return null;
  },
  contactsByAlphabet,
  importContactsDialogOpen: state => Object.keys(state.importContacts.contacts).length > 0,
});

const mapDispatchToProps = (dispatch, props) => ({
  callTo: contact => () => {
    dispatch(actions.change('call.number', contact.number));
    dispatch(actions.submit('call'));
  },
  removeContact: () => {
    dispatch(api.actions.removeContact(props.contactToRemove.id))
      .then(_ => props.setContactToRemove(null));
  },
  parseContactsFile: file => {
    const request = dispatch(api.actions.parseContactsFile(file))
      .then(contacts => dispatch(actions.change('importContacts.contacts', contacts)))
      .catch(standardErrorHandling);
    dispatch(actions.submit('importContacts', request, { fields: true }));
  },
  resetImportContactsForm: () => dispatch(actions.reset('importContacts')),
});

export default compose(
  withState('contactToRemove', 'setContactToRemove', null),
  withState('importContactsDialogOpen', 'setImportContactsDialogState', false),
  connect(selector, mapDispatchToProps),
);
