import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import MakeACall from 'widgets/make-a-call';
import { Link } from 'react-router-dom';
import ContactsDivider from 'components/contacts-divider';
import VectorIcon from 'vector-icon';
import moment from 'moment';
import 'moment-timezone';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableFooter,
} from 'material-ui/Table';
import AlertBox from 'components/alert-box';
import SearchForm from 'components/search-form';
import FlatButton from 'material-ui/FlatButton';
import ContactsImporter from 'widgets/contacts-importer';
import UploadFileButton from 'components/upload-file-button';
import Snackbar from 'material-ui/Snackbar';

const flags = {
  au: require('assets/images/flags/au.png'),
  ca: require('assets/images/flags/ca.png'),
  us: require('assets/images/flags/us.png'),
  gb: require('assets/images/flags/gb.png'),
  ie: require('assets/images/flags/ie.png'),
};

const Contacts = ({
  currentUser,
  contactsByAlphabet,
  callTo,
  contactToRemove,
  setContactToRemove,
  removeContact,
  match,
  importContactsDialogOpen,
  resetImportContactsForm,
  parseContactsFile,
  contactsParserError,
}) => {
  function renderContacts() {
    const rows = [];
    Object.keys(contactsByAlphabet).map(key => {
      rows.push(<ContactsDivider key={key} letter={key} colSpan={5} />);
      rows.push(renderContactsArray(contactsByAlphabet[key]));
    });
    return rows;
  }
  function renderContactsArray(contacts) {
    return contacts.map(entity =>
      <TableRow selectable={false} displayBorder={false} key={entity.id} id={`contact-${entity.id}`}>
        <TableRowColumn style={{ paddingLeft: 8, paddingRight: 0 }}>
          <VectorIcon
            name="handsetOnGreen"
            className="handset-on-green"
            onClick={callTo(entity)}
          />
        </TableRowColumn>
        <TableRowColumn style={{ paddingLeft: 0 }}>
          {entity.name}
        </TableRowColumn >
        <TableRowColumn>
          {entity.number}
        </TableRowColumn>
        <TableRowColumn>
          {entity.country && flags[entity.country.toLowerCase()] &&
            <img src={flags[entity.country.toLowerCase()]} style={{ marginRight: 12 }} className="flag-icon" />
          }
          <span style={{ color: '#a1a1a1' }}>Local Time:</span> {moment.tz(new Date(), entity.timezone).format('h:mm A')}
        </TableRowColumn>
        <TableRowColumn style={{ textAlign: 'center' }}>
          <VectorIcon
            name="trash"
            style={{ cursor: 'pointer' }}
            onClick={() => setContactToRemove(entity)}
          />
        </TableRowColumn>
      </TableRow>);
  };
  return <ManagerDashboard>
    <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
      <SubMenu role={currentUser.role} path={match.path} />
      <SearchForm model="searchContacts" />
    </div>
    <div />
    <div>
      <div className="shadow">
        <Table
          className="contacts-table"
          wrapperStyle={{ backgroundColor: '#fff', minWidth: 870 }}
          bodyStyle={{ minWidth: 870 }}
          height="calc(100vh - 221px)"
          fixedHeader
          fixedFooter
        >
          <TableBody
            displayRowCheckbox={false}
            className="first-row-padding"
            showRowHover
            stripedRows
          >
            {renderContacts()}
            {currentUser.contacts.length === 0 && <TableRow>
              <TableRowColumn style={{ textAlign: 'center' }}>Contacts list is empty</TableRowColumn>
            </TableRow>}
          </TableBody>
          <TableFooter adjustForCheckbox={false}>
            <TableRow>
              <TableRowColumn colSpan={5} style={{ textAlign: 'right' }}>
                <UploadFileButton label="import" upload={parseContactsFile} />
              </TableRowColumn>
            </TableRow>
          </TableFooter>
        </Table>
      </div>
      <AlertBox
        open={!!contactToRemove}
        onRequestClose={() => setContactToRemove(null)}
        yesAction={removeContact}
      >
        Remove contact <b>{contactToRemove && contactToRemove.name}</b> ?
      </AlertBox>
      <ContactsImporter
        open={importContactsDialogOpen}
        onRequestClose={resetImportContactsForm}
      />
      {contactsParserError && <Snackbar
        open={!!contactsParserError}
        message={contactsParserError}
        autoHideDuration={10000}
        onRequestClose={resetImportContactsForm}
        contentStyle={{
          color: 'rgb(255, 64, 129)',
        }}
      />}
    </div>
    {currentUser.forwardingNumber && currentUser.forwardingNumber.confirmed
      ? <MakeACall />
      : <FlatButton
        containerElement={<Link to="/my-numbers" />}
        label="Confirm Outbound Number"
        fullWidth
        secondary
      />
    }
  </ManagerDashboard>;
};

const ManagerDashboard = styled.div`
  display: grid;
  grid-template-columns: 1fr 288px;
  grid-column-gap: 56px;
`;

Contacts.propTypes = {
  currentUser: PropTypes.object.isRequired,
  contactsByAlphabet: PropTypes.object.isRequired,
  callTo: PropTypes.func.isRequired,
  contactToRemove: PropTypes.object,
  setContactToRemove: PropTypes.func.isRequired,
  removeContact: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  importContactsDialogOpen: PropTypes.bool.isRequired,
  resetImportContactsForm: PropTypes.func.isRequired,
  parseContactsFile: PropTypes.func.isRequired,
  contactsParserError: PropTypes.any,
};

export default smartify(Contacts);
