import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import VectorIcon from 'vector-icon';
import smartify from './smartify';
import { Form } from 'react-redux-form';
import InputText from 'components/input-text';
import ErrorBox from 'components/error-box';
import NotifyBox from 'components/notify-box';
import DialogBoxButton from 'components/dialog-box/button';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const ForwardingNumber = ({
  submit,
  currentUser,
  selectedRep,
  setRep,
}) => <div>
  <div className="my-numbers-header">
    <div>Outbound Numbers</div>
    <div className="my-numbers-header-help">
      Inbound calls to purchased numbers will be forwarded to rep outbound numbers
    </div>
  </div>
  <Table
    height="calc(100vh - 340px)"
    wrapperStyle={{
      borderRadius: 5,
    }}
  >
    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
      <TableRow
        style={{
          borderTop: '1px solid #fafafa',
          borderBottom: '1px solid #fafafa',
        }}
      >
        <TableHeaderColumn><UserIcon name="user" style={{ position: 'relative', bottom: -2, left: -5 }} />Rep name</TableHeaderColumn>
        <TableHeaderColumn><VectorIcon name="call" style={{ position: 'relative', bottom: -2, left: -5 }} />Outbound Number</TableHeaderColumn>
      </TableRow>
    </TableHeader>
    <TableBody displayRowCheckbox={false}>
      {currentUser.salesReps.map(rep =>
        <TableRow
          key={rep.id}
          style={{
            height: 99,
            borderBottom: '1px solid #fafafa',
          }}
          selectable={false}
        >
          <TableRowColumn style={{
            fontSize: 20,
            letterSpacing: 0.8,
            color: '#5cbece',
          }}>
            {rep.name}
          </TableRowColumn>
          <TableRowColumn style={{
            fontSize: 20,
            letterSpacing: 0.8,
            color: '#5cbece',
          }}>
            <OutboundNumber
              title={rep.number}
              onClick={() => setRep(rep)}
            >
              {rep.number}
              <VectorIcon name="edit" />
            </OutboundNumber>
          </TableRowColumn>
        </TableRow>
      )}
    </TableBody>
  </Table>
  <NotifyBox
    open={!!selectedRep}
    onRequestClose={() => setRep(null)}
    overlayStyle={{
      opacity: 1,
    }}
    actions={[
      <DialogBoxButton
        key="cancel"
        label="cancel"
        onClick={() => setRep(null)}
        hoverColor="#eaeaea"
        style={{borderRight: '1px solid #cecece'}}
      />,
      <DialogBoxButton
        key="change"
        label="change"
        type="submit"
        hoverColor="#eaeaea"
        form="outbound-number-form"
      />,
    ]}
  >
    Change outbound number for <b>{selectedRep && selectedRep.name}</b>
    <Form id="outbound-number-form" model="rep" onSubmit={submit(selectedRep)}>
      <InputText
        model=".rep.forwardingNumberAttributes.number"
        defaultValue={selectedRep && selectedRep.number}
        fullWidth
        required
      />
    </Form>
    <ErrorBox model="rep.commonErrors" show />
  </NotifyBox>
</div>;

ForwardingNumber.propTypes = {
  currentUser: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  selectedRep: PropTypes.any,
  setRep: PropTypes.func,
};

const OutboundNumber = styled.div`
  cursor: pointer;
  > svg {
    margin-left: 8px;
    path {
      opacity: 0;
    }
  }
  &:hover {
    > svg {
      path {
        opacity: 1;
      }
    }
  }
`;

const UserIcon = styled(VectorIcon)`
  > path {
    fill: #000;
    opacity: .4;
  }
`;

export default smartify(ForwardingNumber);
