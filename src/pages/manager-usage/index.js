import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import VectorIcon from 'vector-icon';
import { Link } from 'react-router-dom';
import moment from 'moment';
import FlatButton from 'material-ui/FlatButton';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  TableFooter,
} from 'material-ui/Table';

const ManagerUsage = ({
  currentUser,
  stats,
  year,
  month,
  match,
}) => {
  function navLink(direction = 'next') {
    const act = direction === 'next' ? 'add' : 'subtract';
    const dd = moment().year(year).month(month - 1)[act](1, 'month');
    return `/manager-usage/${dd.year()}/${dd.month() + 1}`;
  }
  function humanizeSeconds(seconds) {
    if (typeof seconds === 'undefined') return '';
    if (seconds === 0) return 0;
    return Math.ceil(seconds / 60);
    // return moment.duration(seconds, 'seconds').humanize();
  }
  function hasPrevStat() {
    const userCreatedAt = new Date(stats.createdAt);
    const currentMonth = moment().year(year).month(month - 1).startOf('month').toDate();
    return userCreatedAt < currentMonth;
  }
  function hasNextStat() {
    const currentMonth = moment().year(year).month(month - 1).endOf('month').toDate();
    return new Date() > currentMonth;
  }
  return <div>
    <SubMenu role={currentUser.role} path={match.path} />
    <div>
      <div className="shadow">
        <Table
          className="manager-usage-table"
          height="calc(100vh - 325px)"
          bodyStyle={{ minWidth: 870 }}
          wrapperStyle={{ minWidth: 870 }}
          fixedHeader
          fixedFooter
        >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn colSpan={9}>
                <div style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}>
                  {hasPrevStat() && <FlatButton
                    icon={<VectorIcon name="chevronLeft" />}
                    containerElement={<Link to={navLink('prev')} />}
                    secondary
                  />}
                  {!hasPrevStat() && DummyButton}
                  <FlatButton
                    label={<span>{moment().year(year).month(month - 1).format('MMMM YYYY')}</span>}
                    disabled
                  />
                  {hasNextStat() && <FlatButton
                    icon={<VectorIcon name="chevronLeft" style={{ transform: 'rotate(.5turn)' }} />}
                    containerElement={<Link to={navLink('next')} />}
                    secondary
                  />}
                  {!hasNextStat() && DummyButton}
                </div>
              </TableHeaderColumn>
            </TableRow>
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Local<br />Numbers</TableHeaderColumn>
              <TableHeaderColumn>Incoming<br />min/mon</TableHeaderColumn>
              <TableHeaderColumn>Outgoing<br />min/mon</TableHeaderColumn>
              <TableHeaderColumn>Recordings<br />min/mon</TableHeaderColumn>
              <TableHeaderColumn>Transcripts<br />min/mon</TableHeaderColumn>
              <TableHeaderColumn>Total memory<br />of recordings</TableHeaderColumn>
              <TableHeaderColumn>Toll-Free<br />Numbers</TableHeaderColumn>
              <TableHeaderColumn>Status</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover stripedRows>
            {stats.salesReps.map(rep => {
              return <TableRow
                key={rep.id}
                displayBorder={false}
              >
                <TableRowColumn>
                  {rep.name}
                </TableRowColumn>
                <TableRowColumn>
                  {rep.localNumbersCount}
                </TableRowColumn>
                <TableRowColumn>
                  {humanizeSeconds(rep.durationIncoming)}
                </TableRowColumn>
                <TableRowColumn>
                  {humanizeSeconds(rep.durationOutbound)}
                </TableRowColumn>
                <TableRowColumn>
                  {humanizeSeconds(rep.recordingsDuration)}
                </TableRowColumn>
                <TableRowColumn>
                  {humanizeSeconds(rep.transcriptionsDuration)}
                </TableRowColumn>
                <TableRowColumn>
                  {rep.recordsSize}
                </TableRowColumn>
                <TableRowColumn>
                  {rep.tollFreeNumbersCount}
                </TableRowColumn>
                <TableRowColumn>
                  {rep.status || 'Active'}
                </TableRowColumn>
              </TableRow>;
            })}
            {stats.salesReps.length === 0 && <TableRow>
              <TableRowColumn style={{ textAlign: 'center' }}>You have no info to display.</TableRowColumn>
            </TableRow>}
          </TableBody>
          <TableFooter adjustForCheckbox={false}>
            <TableRow>
              <TableRowColumn>
                <b>Total</b>
              </TableRowColumn>
              <TableRowColumn>
                {stats.localNumbersCount}
              </TableRowColumn>
              <TableRowColumn>
                {humanizeSeconds(stats.durationIncoming)}
              </TableRowColumn>
              <TableRowColumn>
                {humanizeSeconds(stats.durationOutbound)}
              </TableRowColumn>
              <TableRowColumn>
                {humanizeSeconds(stats.recordingsDuration)}
              </TableRowColumn>
              <TableRowColumn>
                {humanizeSeconds(stats.transcriptionsDuration)}
              </TableRowColumn>
              <TableRowColumn>
                {stats.recordsSize}
              </TableRowColumn>
              <TableRowColumn>
                {stats.tollFreeNumbersCount}
              </TableRowColumn>
              <TableRowColumn />
            </TableRow>
          </TableFooter>
        </Table>
      </div>
    </div>
  </div>;
};

const DummyButton = <FlatButton
  label={<span>&mdash;</span>}
  disabled
/>;

ManagerUsage.propTypes = {
  currentUser: PropTypes.object,
  stats: PropTypes.object.isRequired,
  year: PropTypes.number,
  month: PropTypes.number,
  match: PropTypes.object.isRequired,
};

export default smartify(ManagerUsage);
