import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import AddAnAdmin from 'widgets/add-an-admin';
import SubMenu from 'components/sub-menu';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SearchForm from 'components/search-form';

const Admins = ({
  currentUser,
  admins,
  match,
}) =>
  <ManagerDashboard>
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', paddingRight: 24 }}>
        <SubMenu role={currentUser.role} path={match.path} />
        <SearchForm model="searchAdmins" />
      </div>
      <div className="shadow">
        <Table
          className="admins-table responsive-table"
          fixedHeader
        >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Email</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} stripedRows>
            {admins.map(entity =>
              <TableRow
                key={entity.id}
                displayBorder={false}
                selectable={false}
              >
                <TableRowColumn>
                  <span className="responsive-table-subname">Name</span>
                  {entity.name}
                </TableRowColumn>
                <TableRowColumn>
                  <span className="responsive-table-subname">Email</span>
                  {entity.email}
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    </div>
    <AddAnAdmin />
  </ManagerDashboard>;

const ManagerDashboard = styled.div`
  display: grid;
  grid-template-columns:minmax(300px,1fr) 288px;
  grid-column-gap: 56px;
  @media (max-width: 991px) {
    grid-template-columns:1fr;
  }
`;

Admins.propTypes = {
  currentUser: PropTypes.object.isRequired,
  admins: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
};

export default smartify(Admins);
