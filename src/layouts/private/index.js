import React from 'react';
import PropTypes from 'prop-types';
import smartify from './smartify';
import styled from 'styled-components';
import MainMenu from 'components/main-menu';
import Header from 'components/header';
import Helmet from 'react-helmet';

const PrivateLayout = ({
  loginComponent,
  logout,
  loggedAs,
  backToMainAccount,
  ...rest
}) => {
  if (!rest.currentUser) return <div>{React.createElement(loginComponent, rest)}</div>;
  const mainMenu = <MainMenu
    role={rest.currentUser.role}
    logout={logout}
    loggedAs={loggedAs}
    backToMainAccount={backToMainAccount}
  />;
  return <Container>
    <Helmet title={rest.title} />
    <Header
      mainMenu={mainMenu}
      loggedAs={loggedAs}
    />
    <Wrapper>
      {(() => {
        switch (rest.currentUser.role) {
          case 'admin': return React.createElement(rest.adminPage || rest.page, rest);
          case 'manager': return React.createElement(rest.managerPage || rest.page, rest);
          default: return React.createElement(rest.page, rest);
        }
      })()}
    </Wrapper>
  </Container>;
};

const Container = styled.div`
  height: 100vh;
`;

const Wrapper = styled.div`
  padding: 2rem 5%;
`;

PrivateLayout.propTypes = {
  page: PropTypes.any,
  managerPage: PropTypes.any,
  adminPage: PropTypes.any,
  loginComponent: PropTypes.any.isRequired,
  logout: PropTypes.func.isRequired,
  loggedAs: PropTypes.string,
  backToMainAccount: PropTypes.func.isRequired,
};

export default smartify(PrivateLayout);
